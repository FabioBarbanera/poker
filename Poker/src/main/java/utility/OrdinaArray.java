package utility;

import giocatore.mano.Carta;
import giocatore.mano.Mano;

public class OrdinaArray {

	private OrdinaArray() {};
	
	public static void ordina(Mano mano) {
		
		for(int i=0; i<mano.getCarteDellaMano().length; i++) {
			for(int j=i+1; j<mano.getCarteDellaMano().length; j++) {
				if(mano.getCarteDellaMano()[i].getValoreCarta().getValore() > mano.getCarteDellaMano()[j].getValoreCarta().getValore()) {
					Carta temp = mano.getCarteDellaMano()[i];
					mano.getCarteDellaMano()[i] = mano.getCarteDellaMano()[j];
					mano.getCarteDellaMano()[j] = temp;
				}
			}
		}
		
	}
}
