package utility;

import java.util.Random;

public class NumeroRandom {

	private NumeroRandom() {
	}

	public static Integer numeroRandomGiocatori() {

		Random rand = new Random();
		return rand.nextInt((5 - 2) + 1) + 2;
		
	}

	public static Integer numeroRandomIndiceListaMazzoDiCarte() {

		Random rand = new Random();
		return rand.nextInt(52 - 1) + 1;
		
	}

}
