package implMano.controllaPunteggioMano;

import java.util.ArrayList;
import java.util.List;

import giocatore.mano.Carta;
import giocatore.mano.Mano;

public class ControllaPunteggioCoppia {

	private Mano mano;
	private List<Carta> cartekicker = new ArrayList<Carta>();

	public ControllaPunteggioCoppia(Mano mano) {
		this.mano=mano;
	}
	
	public List<Carta> valuta() {
		
		List<Carta>carteCoppia= new ArrayList<Carta>();
		int contatoreUguaglianze=0;
		
		for(int i=0; i<this.mano.getCarteDellaMano().length; i++) {
			for(int j=i+1; j<this.mano.getCarteDellaMano().length; j++) {
				if(this.mano.getCarteDellaMano()[i].getValoreCarta().getValore()==this.mano.getCarteDellaMano()[j].getValoreCarta().getValore()) {
					contatoreUguaglianze++;
					carteCoppia.add(this.mano.getCarteDellaMano()[i]);
					carteCoppia.add(this.mano.getCarteDellaMano()[j]);
				}
			}
		}
		for(int i=0; i<this.mano.getCarteDellaMano().length; i++) {
			
			if(carteCoppia.size()==2) {
				if(carteCoppia.get(0).getValoreCarta().getValore()!=this.mano.getCarteDellaMano()[i].getValoreCarta().getValore()) {
					cartekicker.add(this.mano.getCarteDellaMano()[i]);
				}
			}
		}
		
		if(contatoreUguaglianze==1 && carteCoppia.size()==2) {
			return carteCoppia;
		}
		
		return null;
		
	}
	
	public List<Carta> getCarteKicker() {
		return cartekicker;
	}

}
