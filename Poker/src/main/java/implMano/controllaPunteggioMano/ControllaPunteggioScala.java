package implMano.controllaPunteggioMano;

import controllaEsitoPartita.esito.EsitoManoGiocatore;
import giocatore.mano.Carta;
import giocatore.mano.Mano;
import implMano.controllaPunteggioMano.risultatoMano.Scala;
import implMano.controllaPunteggioMano.risultatoMano.ScalaColore;
import implMano.controllaPunteggioMano.risultatoMano.ScalaReale;
import utility.OrdinaArray;

public class ControllaPunteggioScala{

	private Mano mano;
	
	public  ControllaPunteggioScala(Mano mano) {
		this.mano=mano;
	}

	public EsitoManoGiocatore valuta() {
		
		/*
		 * Ordino l'array per valore in ordine crescente 
		 */
		OrdinaArray.ordina(mano);
		
		/*
		 * Scale:
		 * -Scala reale: sequenza da A a 5 stesso seme oppure da 10 ad A stesso seme
		 * -Scala colore: qualsiasi altra scala mediana con stesso seme
		 * -Scala: scala di valori con carte di semi diversi
		 */
		
		Carta[] arraySoloScalaValore = new Carta[mano.getCarteDellaMano().length];
		Carta[] arrayScalaColore = new Carta[mano.getCarteDellaMano().length];
		Carta[] arrayScalaReale = new Carta[mano.getCarteDellaMano().length];
		int contatoreValore = 0;
		
		for(int i=0; i< mano.getCarteDellaMano().length; i++) {
			for(int j=i+1; j< mano.getCarteDellaMano().length;) {
				contatoreValore=j;
				/*
				 * Se il valore alla posizione precedente confrontato con quello successivo sono diversi 
				 * esco direttamente da ciclo interno
				 */
				if(mano.getCarteDellaMano()[j].getValoreCarta().getValore() != (mano.getCarteDellaMano()[i].getValoreCarta().getValore()+1)) {
					break;
					/*
					 * Se invece il numero contenuto nella posizione dell'array successivo è un valore in più a quello precedente
					 * significa che posso continuare a verificare se effewttivamente si può fare scala
					 */
				}else {
					arraySoloScalaValore[i] = mano.getCarteDellaMano()[i];
					arraySoloScalaValore[j] = mano.getCarteDellaMano()[j];
					break;
				}
			}
			/*
			 * Se sono uscito dal ciclo interno a causa di questa condizione significa che non posso fare scala a priori
			 * Quindi esco anche dal ciclo esterno
			 */
			if(mano.getCarteDellaMano()[contatoreValore].getValoreCarta().getValore() != (mano.getCarteDellaMano()[i].getValoreCarta().getValore()+1)) {
				break;
			}
		}
		/*
		 * Alla fine di questo pezzo di algoritmo se effettivamente c'è la scala l'array 
		 * arraySoloScalaValore conterra la scala o parte di essa
		 */
		
		
		/*
		 * In base alla lunghezza dell'array(cioè se fino al penultimo posto l'array è stato riempito)
		 * posso capire se c'è possibilita di fare scala reale
		 * devo verificare situazione in cui l'array scala è lungo 4 e all'utlima posizione
		 * si trova l'asso e la prima posizion è 10 o 2.
		 * Se è 10 posso fare scala reale con asso 14 se c'è 2 posso fare scala reale con asso 1
		 * Asso che, nel caso della scala può valere anche 1
		 * Dato che come valore iniziale gli è stato dato 14 devo verificare soltanto la condizione
		 * in cui può valere 1
		 * 
		 * Verificare se effettivamente nell'utlima posizione dell'array
		 * si trovi l'asso e che nelle altre posizioni vi siano i valori: 2,3,4,5
		 */
		
	if(arraySoloScalaValore[3]!=null) {
		if(mano.getCarteDellaMano()[4].getValoreCarta().getCarattereValore().equals("A")
				&& mano.getCarteDellaMano()[4].getValoreCarta().getValore()==14 && (arraySoloScalaValore[3].getValoreCarta().getValore()==13
				||arraySoloScalaValore[3].getValoreCarta().getValore()==5
				&&arraySoloScalaValore[0].getValoreCarta().getValore()==2 ||arraySoloScalaValore[0].getValoreCarta().getValore()==10)
				) {//controllare condizione perche con 8,9,10,j passa questa verifica aggiungere posizione 1 minimo 10 o 2
			
			/*
			 * Se la condizione precedente è verificata significa che l'arraysoloscalavalori 
			 * è stato riempito fino alla penultima posizione perché l'ultima posizione della mano 
			 * c'è l'asso che di default vale 14 allora imposto il suo valore a 1
			 * quindi lo riordino e al primo posto ora ci sarà l'asso con valore 1
			 */
			if(arraySoloScalaValore[0].getValoreCarta().getValore()==2 && arraySoloScalaValore[3].getValoreCarta().getValore()==5
					 && mano.getCarteDellaMano()[4].getValoreCarta().getCarattereValore().equals("A")) {
				mano.getCarteDellaMano()[4].getValoreCarta().setValore(1);
				OrdinaArray.ordina(mano);
			}
			
			
			/*
			 * Quindi posso assegnare all'array di solo scala valori gli stessi valorei della relativa posizione 
			 * della mano
			 */
			for (int i=0; i<arraySoloScalaValore.length; i++) {
				arraySoloScalaValore[i] = mano.getCarteDellaMano()[i];
			}

			/*
			 * Vado a controllare ora i semi della scala così da vedere se può essere scala reale
			 */
			int contatoreSeme = 0;
				for(int i=0; i<arraySoloScalaValore.length; i++) {
					for(int j=i+1; j<arraySoloScalaValore.length; j++) {
						contatoreSeme=j;
						
						/*
						 *Se alla prima posizone vi è 10 o 1 e i semi della posizione precedente e quella successiva sono uguali
						 *significa che c'è possibilità di fare scala reale
						 */
						if(arraySoloScalaValore[i].getSeme().getSeme().equals(arraySoloScalaValore[j].getSeme().getSeme())
								&&(arraySoloScalaValore[0].getValoreCarta().getValore()==10||arraySoloScalaValore[0].getValoreCarta().getValore()==1)) {
							
							/*
							 * Quindi mi vado a riempire l'array della scala reale con i valori che rispettano la condizione precendente
							 */
							arrayScalaReale[i]=arraySoloScalaValore[i];
							arrayScalaReale[j]=arraySoloScalaValore[j];
							
							/*
							 * Se i vale 3 e j 4 significa che ho fatto tutti i confronti possibili quindi posso uscire
							 */
							if(i==3 && j==4) {
								break;
							}
							/*
							 * Se la condizione del if precedente non si è verificata anche solo una volta significa che non 
							 * c'è possibilità di fare scala reale, quindi posso uscire
							 */
						}else {
							break;
						}
					}
					
					/*
					 * Se quindi non si è verificata la condizione di uguaglianza tra seme della posizione precedente con quello della posizone successiva
					 * sicuramente non posso fare scala reale ed esco anche dal ciclo esterno
					 */
					if(!arraySoloScalaValore[i].getSeme().getSeme().equals(arraySoloScalaValore[contatoreSeme].getSeme().getSeme())) {
						break;
					}
				}
				
				/*
				 * Altrimenti verifico se è scala colore 
				 */
		}else {
			int contatoreColore = 0;
			for(int i=0; i<arrayScalaColore.length; i++) {
				for(int j=i+1; i<arrayScalaColore.length;) {
					contatoreColore=j;
					if(arraySoloScalaValore[i]!=null && arraySoloScalaValore[j]!=null) {
					if((arraySoloScalaValore[0].getValoreCarta().getValore()!=1 || arraySoloScalaValore[4].getValoreCarta().getValore()!=14) 
							&& arraySoloScalaValore[i].getSeme().getSeme().equals(arraySoloScalaValore[j].getSeme().getSeme())) {
						
						arrayScalaColore[i]=arraySoloScalaValore[i];
						arrayScalaColore[j]=arraySoloScalaValore[j];
						break;
					}else {
						break;
					}
					
					}else {
						break;
					}
				}
				if(i==3 && contatoreColore==4) {
					break;
				}
			}
		}
		
	}
	
	/*
	 * Ora mettere condizione che verificando se Array arrayScalaReale è != da null aumenta un contatore 
	 * Se il contatore è quanto la lunghezza dell'array significa che è possibile creare un 'oggetto di quel tipo
	 * altrimenti faccio la stessa verifica su arrayScalaColore e se non è su arrayScalaSoloValore
	 */
	int contatoreScalaReale = 0;
	int contatoreSoloScalaValore = 0;
	int contatoreScalaColore = 0;
	
	for(Carta carta : arrayScalaReale) {
		if(carta!=null) {
			contatoreScalaReale++;
		}
	}
	
	for (Carta carta : arrayScalaColore) {
		if(carta!=null) {
			contatoreScalaColore++;
		}
	}
	
	for (Carta carta : arraySoloScalaValore) {
		if(carta!=null) {
			contatoreSoloScalaValore++;
		}
	}
	
	if(contatoreScalaReale==5) {
//		return new ScalaReale(mano);
		return new EsitoManoGiocatore(new ScalaReale(new Mano(arrayScalaReale)));
	}else if(contatoreScalaColore==5 && contatoreSoloScalaValore==5) {
//		return new ScalaColore(mano);
		return new EsitoManoGiocatore(new ScalaColore(new Mano(arrayScalaColore)));
		
	}else if((contatoreSoloScalaValore==5) && (contatoreScalaReale != 5 || contatoreScalaColore != 5)) {
//		return new Scala(mano);
		return new EsitoManoGiocatore(new Scala(new Mano(arraySoloScalaValore)));
				
	}else {
		return null;
	}
	
	
	}
}
