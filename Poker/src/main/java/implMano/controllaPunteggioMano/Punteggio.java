package implMano.controllaPunteggioMano;


import controllaEsitoPartita.esito.EsitoManoGiocatore;
import giocatore.Giocatore;
import implMano.controllaPunteggioMano.risultatoMano.CartaPiuAlta;
import implMano.controllaPunteggioMano.risultatoMano.Colore;
import implMano.controllaPunteggioMano.risultatoMano.Coppia;
import implMano.controllaPunteggioMano.risultatoMano.DoppiaCoppia;
import implMano.controllaPunteggioMano.risultatoMano.Full;
import implMano.controllaPunteggioMano.risultatoMano.Poker;
import implMano.controllaPunteggioMano.risultatoMano.Tris;

public class Punteggio {
	
	private Giocatore giocatore;
	
	public Punteggio(Giocatore giocatore){
		this.giocatore=giocatore;
	}
	
	public EsitoManoGiocatore valuta() {
		
		ControllaPunteggioScala controlloPunteggioScala = new ControllaPunteggioScala(this.giocatore.getManoGiocatore());
		if(controlloPunteggioScala.valuta()!=null) {
			EsitoManoGiocatore esitoManoScala = controlloPunteggioScala.valuta();
			return esitoManoScala;
		}
		
		ControllaPunteggioPoker controlloPunteggioPoker = new ControllaPunteggioPoker(this.giocatore.getManoGiocatore());
		if(controlloPunteggioPoker.valuta()!=null) {
			return new EsitoManoGiocatore(new Poker(controlloPunteggioPoker.valuta(), controlloPunteggioPoker.getCartaPokerKicker(), this.giocatore.getManoGiocatore()));
		}
		
		ControllaPunteggioFull controlloPunteggioFull = new ControllaPunteggioFull(this.giocatore.getManoGiocatore());
		if(controlloPunteggioFull.valuta()!=null) {
			return new EsitoManoGiocatore(new Full(controlloPunteggioFull.valuta(), this.giocatore.getManoGiocatore()));
		}
		
		
		ControllaPunteggioColore controlloPunteggioColore = new ControllaPunteggioColore(this.giocatore.getManoGiocatore());
		if(controlloPunteggioColore.valuta()!=null) {
			return new EsitoManoGiocatore(new Colore(controlloPunteggioColore.valuta(), this.giocatore.getManoGiocatore()));
		}
		
		
		ControllaPunteggioTris controlloPunteggioTris = new ControllaPunteggioTris(this.giocatore.getManoGiocatore());
		if(controlloPunteggioTris.valuta()!=null) {
			return new EsitoManoGiocatore(new Tris(controlloPunteggioTris.valuta(), controlloPunteggioTris.getCarteKicker(), this.giocatore.getManoGiocatore()));
		}
		
		
		ControllaPunteggioDoppiaCoppia controlloPunteggioDoppiaCoppia = new ControllaPunteggioDoppiaCoppia(giocatore.getManoGiocatore());
		if(controlloPunteggioDoppiaCoppia.valuta()!=null) {
			return new EsitoManoGiocatore(new DoppiaCoppia(controlloPunteggioDoppiaCoppia.valuta(), controlloPunteggioDoppiaCoppia.getCartaKicker(), this.giocatore.getManoGiocatore()));
		}
		
		ControllaPunteggioCoppia controlloPunteggioCoppia = new ControllaPunteggioCoppia(giocatore.getManoGiocatore());
		if(controlloPunteggioCoppia.valuta()!=null) {
			return new EsitoManoGiocatore(new Coppia(controlloPunteggioCoppia.valuta(), controlloPunteggioCoppia.getCarteKicker(), this.giocatore.getManoGiocatore()));
		}
		
		
		ControllaPunteggioCartaPiuAlta controlloPunteggioCartaPiuAlta = new ControllaPunteggioCartaPiuAlta(this.giocatore.getManoGiocatore());
		if(controlloPunteggioCartaPiuAlta.valuta()!=null) {
			return new EsitoManoGiocatore(new CartaPiuAlta(this.giocatore.getManoGiocatore()));
		}
		
		return null;
		
	}
}
