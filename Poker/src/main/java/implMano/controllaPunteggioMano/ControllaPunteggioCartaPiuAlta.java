package implMano.controllaPunteggioMano;

import giocatore.mano.Carta;
import giocatore.mano.Mano;

public class ControllaPunteggioCartaPiuAlta {

	private Mano mano;

	public ControllaPunteggioCartaPiuAlta(Mano mano) {
		this.mano=mano;
	}
	
	public Carta valuta() {
		return this.mano.getCarteDellaMano()[4];
	}
	
	public Mano getMano() {
		return mano;
	}

}
