package implMano.controllaPunteggioMano;

import java.util.ArrayList;
import java.util.List;

import giocatore.mano.Carta;
import giocatore.mano.Mano;

public class ControllaPunteggioTris{
	
	private Mano mano;
	private List<Carta> carteKicker = new ArrayList<Carta>();

	public ControllaPunteggioTris(Mano mano) {
		this.mano=mano;
	}

	public List<Carta> valuta() {
		
		List<Carta>carteTris = new ArrayList<Carta>();
		int contatoreUguaglianza=0;
		
		for(int i=0; i<this.mano.getCarteDellaMano().length;i++) {
			for(int j=i+1; j<this.mano.getCarteDellaMano().length; j++) {
				if(this.mano.getCarteDellaMano()[i].getValoreCarta().getValore()==this.mano.getCarteDellaMano()[j].getValoreCarta().getValore()) {
					contatoreUguaglianza++;
					carteTris.add(this.mano.getCarteDellaMano()[i]);
					carteTris.add(this.mano.getCarteDellaMano()[j]);
				}
			}
		}
		
		for(int i=0; i<carteTris.size(); i++) {
			for(int j=i+1; j<carteTris.size(); j++) {
				if(carteTris.get(i).equals(carteTris.get(j))) {
					carteTris.remove(j);
				}
			}
		}
		
		for(int i=0; i<this.mano.getCarteDellaMano().length; i++) {
			
			if(carteTris.size()==3) {
				if(carteTris.get(0).getValoreCarta().getValore()!=this.mano.getCarteDellaMano()[i].getValoreCarta().getValore()) {
					this.carteKicker.add(this.mano.getCarteDellaMano()[i]);
				}
			}
		}
		
		if(contatoreUguaglianza==3 && carteTris.size()==3) {
			return carteTris;
		}
		return null;
	}
	
	public List<Carta> getCarteKicker() {
		return carteKicker;
	}
	
}
