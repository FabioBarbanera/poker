package implMano.controllaPunteggioMano;

import giocatore.mano.Mano;

public class ControllaPunteggioColore {

	private Mano mano;
	
	public ControllaPunteggioColore(Mano mano) {
		this.mano=mano;
	}

	public Character valuta() {

		//ctrl+1 sull'elemento che da errore ti visualizza l'errore e l'eventuale possibile risoluzione
		int contatoreUguaglianze=0;
		
		for(int i=0; i<this.mano.getCarteDellaMano().length; i++) {
			for(int j=i+1; j<this.mano.getCarteDellaMano().length; j++) {
				if(this.mano.getCarteDellaMano()[i].getSeme().getSeme().equals(this.mano.getCarteDellaMano()[j].getSeme().getSeme())) {
					contatoreUguaglianze++;
				}
			}
		}
		
		if(contatoreUguaglianze==10) {
			return this.mano.getCarteDellaMano()[0].getSeme().getSeme();
		}
//		else {
//			ControllaPunteggioTris controlloTris = new ControllaPunteggioTris(mano);
//			controlloTris.valuta();
//		}
		
		return null;
	}
	
}
