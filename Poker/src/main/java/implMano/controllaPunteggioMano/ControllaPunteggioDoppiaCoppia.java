package implMano.controllaPunteggioMano;

import java.util.ArrayList;
import java.util.List;

import giocatore.mano.Carta;
import giocatore.mano.Mano;

public class ControllaPunteggioDoppiaCoppia {

	private Mano mano;
	private Carta cartaKicker;

	public ControllaPunteggioDoppiaCoppia(Mano mano) {
		this.mano=mano;
	}
	
	public List<Carta> valuta() {
		
		List<Carta>carteDoppiaCoppia = new ArrayList<Carta>();
		
		int contatoreUguaglianze = 0;
		
		for(int i=0; i<this.mano.getCarteDellaMano().length; i++) {
			for(int j=i+1; j<this.mano.getCarteDellaMano().length; j++) {
				if(this.mano.getCarteDellaMano()[i].getValoreCarta().getValore()==this.mano.getCarteDellaMano()[j].getValoreCarta().getValore()) {
					contatoreUguaglianze++;
					carteDoppiaCoppia.add(this.mano.getCarteDellaMano()[i]);
					carteDoppiaCoppia.add(this.mano.getCarteDellaMano()[j]);
				}
			}
		}
		
		for(int i=0; i<this.mano.getCarteDellaMano().length; i++) {
			
			if(carteDoppiaCoppia.size()!=0) {
				if(carteDoppiaCoppia.size()==4
				&&carteDoppiaCoppia.get(0).getValoreCarta().getValore()!=this.mano.getCarteDellaMano()[i].getValoreCarta().getValore()
				&&carteDoppiaCoppia.get(3).getValoreCarta().getValore()!=this.mano.getCarteDellaMano()[i].getValoreCarta().getValore()) {
					this.cartaKicker=this.mano.getCarteDellaMano()[i];
				}
			}
		}
		
		if(contatoreUguaglianze==2 && carteDoppiaCoppia.size()==4) {
			return carteDoppiaCoppia;
		}

		return null;
		
	}
	
	public Carta getCartaKicker() {
		return cartaKicker;
	}

}
