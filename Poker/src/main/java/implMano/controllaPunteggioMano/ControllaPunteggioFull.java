package implMano.controllaPunteggioMano;

import java.util.ArrayList;
import java.util.List;

import giocatore.mano.Carta;
import giocatore.mano.Mano;

public class ControllaPunteggioFull {

	private Mano mano;
	
	public ControllaPunteggioFull(Mano mano) {
		this.mano=mano;
	}

	public List<Carta> valuta() {
		
		/*
		 * Come nel poker serve un contatore che mi terrà il conto delle uguaglianze
		 * Nel caso del full deve essere 4 anziche 6 perche le uguahglianze scorrendo l'array avvengono 4 volte anziche 6 
		 * perche ci sono 3 numeri uguali e 2 diversi da quei 3 ma uguali tra loro quindi faranno 1 uguaglianza tra di loro
		 * piu altre 3 uguaglianze tra di loro gli altri 3 numeri
		 */
		/*
		 * Anche qua mi farò una variabile stringa in cui salvo il carattere del valore 
		 * ma quando, scorro l'array e non è uguale al valore del carattere precedente 
		 * me lo metto in un'altra variabile di tipo stringa che quindi conterrà il secondo valore del full
		 */
		
		int contatoreUguaglianze=0;
		List<Carta> carteFull = new ArrayList<Carta>();
//		String primoValoreCartaFull=null;
//		String secondoValoreCartaFull=null;
		
		for(int i=0; i<this.mano.getCarteDellaMano().length;i++) {
			for(int j=i+1; j<this.mano.getCarteDellaMano().length; j++) {
				if(this.mano.getCarteDellaMano()[i].getValoreCarta().getValore()==this.mano.getCarteDellaMano()[j].getValoreCarta().getValore()) {
					contatoreUguaglianze++;
//					primoValoreCartaFull=this.mano.getCarteDellaMano()[i].getValore().getCarattereValore();
					carteFull.add(this.mano.getCarteDellaMano()[i]);
					carteFull.add(this.mano.getCarteDellaMano()[j]);
				}else {
//					secondoValoreCartaFull=this.mano.getCarteDellaMano()[i].getValore().getCarattereValore();
				}
			}
		}
		for(int i=0; i<carteFull.size(); i++) {
			for(int j=i+1; j<carteFull.size(); j++) {
				if(carteFull.get(i)==carteFull.get(j) && carteFull.get(i)!=null) {
					carteFull.set(j, null);
				}
			}
		}
		
//		for(int i=0; i< carteFull.size(); i++) {
//			for(int j=i+1; j<carteFull.size(); j++) {
//				if(carteFull.get(i)==carteFull.get(j)) {
//					carteFull.remove(i);
//				}
//			}
//		}

		for(int i=0; i<carteFull.size();) {
			if(carteFull.get(i)==null) {
				carteFull.remove(i);
			}else {
				i++;
			}
		}
		
		if(contatoreUguaglianze==4 && carteFull.size()==5) {
			return carteFull;
		}
//		else {
//			ControllaPunteggioColore controlloColore = new ControllaPunteggioColore(this.mano);
//			controlloColore.valuta();
//		}
		
		return null;
		
		
//		int contatoreQuantitaValorePrimaCarta = 0;
//		int contatoreQuantitaValoreSecondaCarta = 0;
//		
//		for(int i=0; i<this.mano.getCarteDellaMano().length;i++) {
//			if(primoValoreCartaFull==this.mano.getCarteDellaMano()[i].getValore().getCarattereValore()) {
//				contatoreQuantitaValorePrimaCarta++;
//			}else {
//				contatoreQuantitaValoreSecondaCarta++;
//			}
//		}
//		
//		if(contatoreQuantitaValorePrimaCarta>contatoreQuantitaValoreSecondaCarta && contatoreUguaglianze==4) {
//			new Full(this.mano, primoValoreCartaFull, contatoreQuantitaValorePrimaCarta, secondoValoreCartaFull, contatoreQuantitaValoreSecondaCarta);
//		}else if(contatoreQuantitaValorePrimaCarta<contatoreQuantitaValoreSecondaCarta && contatoreUguaglianze==4) {
//			new Full(this.mano, secondoValoreCartaFull, contatoreQuantitaValoreSecondaCarta, primoValoreCartaFull, contatoreQuantitaValorePrimaCarta);
//		}else {
//			ControllaPunteggioColore controlloColore = new ControllaPunteggioColore(this.mano);
//			controlloColore.valuta();
//		}
		
	}
	
}
