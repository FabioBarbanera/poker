package implMano.controllaPunteggioMano.risultatoMano;

import giocatore.mano.Mano;

public class Colore {
	
	private Character seme;
	private Mano mano;

	public Colore(Character seme, Mano mano) {
		this.seme=seme;
		this.mano=mano;
		System.out.println(" ");
		System.out.println("Hai fatto Colore con: " + seme);
		System.out.println(" ");
	}
	
	public Character getSeme() {
		return seme;
	}
	
	public Mano getMano() {
		return mano;
	}
	
	@Override
	public String toString() {
		return "Colore " + seme;
	}
}
