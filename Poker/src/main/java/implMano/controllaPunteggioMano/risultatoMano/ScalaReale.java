package implMano.controllaPunteggioMano.risultatoMano;

import giocatore.mano.Carta;
import giocatore.mano.Mano;

public class ScalaReale {

	private Mano mano;
	
	public ScalaReale(Mano mano) {
		this.mano=mano;
		System.out.println(" ");
		System.out.println("Hai fatto Scala Reale con: ");
		for(Carta carta : mano.getCarteDellaMano()) {
			System.out.print(carta+" ");
		}
		System.out.println(" ");
		System.out.println(" ");
	}

	public Mano getMano() {
		return mano;
	}

	@Override
	public String toString() {
		return "ScalaReale " + mano;
	}
	
	
	
}
