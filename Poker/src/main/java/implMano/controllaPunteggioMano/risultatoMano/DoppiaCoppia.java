package implMano.controllaPunteggioMano.risultatoMano;

import java.util.List;

import giocatore.mano.Carta;
import giocatore.mano.Mano;

public class DoppiaCoppia{

	private List<Carta> carteDoppiaCoppia;
	private Carta cartaKicker;
	Mano mano;

	public DoppiaCoppia(List<Carta> carteDoppiaCoppia, Carta cartaKicker, Mano mano) {
		this.carteDoppiaCoppia=carteDoppiaCoppia;
		this.cartaKicker=cartaKicker;
		this.mano=mano;
		System.out.println(" ");
		System.out.println("Hai fatto Doppia Coppia con: ");
		for (Carta carta : carteDoppiaCoppia) {
			System.out.print(carta+" ");
		}
		System.out.println(" ");
		System.out.println(" ");
	}

	public List<Carta> getCarteDoppiaCoppia() {
		return carteDoppiaCoppia;
	}
	
	public Carta getCartaKicker() {
		return cartaKicker;
	}
	
	public Mano getMano() {
		return mano;
	}

	@Override
	public String toString() {
		return "DoppiaCoppia con " + carteDoppiaCoppia;
	}
}
