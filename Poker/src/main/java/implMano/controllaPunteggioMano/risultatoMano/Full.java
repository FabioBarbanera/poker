package implMano.controllaPunteggioMano.risultatoMano;

import java.util.List;

import giocatore.mano.Carta;
import giocatore.mano.Mano;

public class Full {

	private List<Carta> carteFull;
	private Mano mano;

	public Full(List<Carta> carteFull, Mano mano) {
		this.carteFull=carteFull;
		this.mano=mano;
		System.out.println(" ");
		System.out.println("Hai vinto con un full di: ");
		for (Carta carta : carteFull) {
			System.out.print(carta+" ");

		}
		System.out.println(" ");
		System.out.println(" ");
	}

	public List<Carta> getCarteFull() {
		return carteFull;
	}

	public Mano getMano() {
		return mano;
	}

	@Override
	public String toString() {
		return "Full " + carteFull;
	}
	
}
