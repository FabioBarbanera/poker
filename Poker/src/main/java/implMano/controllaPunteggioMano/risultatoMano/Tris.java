package implMano.controllaPunteggioMano.risultatoMano;

import java.util.List;

import giocatore.mano.Carta;
import giocatore.mano.Mano;

public class Tris{
	
	private List<Carta>carteTris;
	private List<Carta>carteKicker;
	private  Mano mano;
	
	public Tris(List<Carta>carteTris, List<Carta>carteKicker, Mano mano) {
		this.carteTris=carteTris;
		this.carteKicker=carteKicker;
		this.mano=mano;
		System.out.println(" ");
		System.out.println("Hai fatto tris con:");
		for(int i=0; i<carteTris.size(); i++) {
				System.out.print(carteTris.get(i)+" ");
		}
		System.out.println(" ");
		System.out.println(" ");
	}

	public List<Carta> getCarteTris() {
		return carteTris;
	}
	
	public List<Carta> getCarteKicker() {
		return carteKicker;
	}
	
	public Mano getMano() {
		return mano;
	}
	
	@Override
	public String toString() {
		return "Tris " + carteTris;
	}
}
