package implMano.controllaPunteggioMano.risultatoMano;

import java.util.List;

import giocatore.mano.Carta;
import giocatore.mano.Mano;

public class Coppia{
	
	private List<Carta> carteCoppia;
	private List<Carta> carteKikcer;
	Mano mano;

	public Coppia(List<Carta> carteCoppia, List<Carta> carteKikcer, Mano mano) {
		this.carteCoppia=carteCoppia;
		this.carteKikcer=carteKikcer;
		this.mano=mano;
		System.out.println(" ");
		System.out.println("Hai fatto Coppia di: ");
		for (Carta carta : carteCoppia) {
			System.out.print(carta+" ");
		}
		System.out.println(" ");
		System.out.println(" ");
	}

	public List<Carta> getCarteCoppia() {
		return carteCoppia;
	}
	
	public List<Carta> getCarteKikcer() {
		return carteKikcer;
	}
	
	public Mano getMano() {
		return mano;
	}
	
	@Override
	public String toString() {
		return "Coppia di " + carteCoppia;
	}
	
	
	
}
