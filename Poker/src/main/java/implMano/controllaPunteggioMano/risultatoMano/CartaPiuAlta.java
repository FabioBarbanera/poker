package implMano.controllaPunteggioMano.risultatoMano;

import giocatore.mano.Carta;
import giocatore.mano.Mano;

public class CartaPiuAlta {
	
	private Mano mano;
	private Carta cartaAlta;

	public CartaPiuAlta(Mano mano) {
		this.mano=mano;
		this.cartaAlta=this.mano.getCarteDellaMano()[4];
		System.out.println(" ");
		System.out.println("Hai Fatto Carta Piu Alta con: "+this.cartaAlta);
		System.out.println(" ");
	}

	@Override
	public String toString() {
		return  ""+mano.getCarteDellaMano()[4];
	}
	
	public Mano getMano() {
		return mano;
	}
	
	public Carta getCartaAlta() {
		return cartaAlta;
	}
	
	/**
	 * Unico controllo di eccezione da fare è controllare che all'interno del mazzo non ci siano 
	 * carte dello stesso tipo
	 */
	
}
