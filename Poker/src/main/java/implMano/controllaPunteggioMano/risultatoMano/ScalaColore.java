package implMano.controllaPunteggioMano.risultatoMano;

import giocatore.mano.Carta;
import giocatore.mano.Mano;

public class ScalaColore {

	private Mano mano;
	
	public ScalaColore(Mano mano) {
		this.mano=mano;
		System.out.println(" ");
		System.out.println("Hai fatto Scala Colore con:");
		for(Carta carta : mano.getCarteDellaMano()) {
			System.out.print(carta+" ");
		}
		System.out.println(" ");
		System.out.println(" ");
	}

	public Mano getMano() {
		return mano;
	}

	@Override
	public String toString() {
		return "ScalaColore " + mano;
	}
	
	
	
}
