package implMano.controllaPunteggioMano.risultatoMano;

import java.util.List;

import giocatore.mano.Carta;
import giocatore.mano.Mano;

public class Poker {

	private List<Carta> cartePoker;
	private Carta cartaPokerKicker;
	private Mano mano;
	
	public Poker(List<Carta> cartePoker, Carta cartaPokerKicker, Mano mano) {
		this.cartePoker=cartePoker;
		this.cartaPokerKicker=cartaPokerKicker;
		this.mano=mano;
		System.out.println(" ");
		System.out.println("Hai vinto con poker di: ");
		for (Carta carta : cartePoker) {
			System.out.print(carta+" ");
		}
		System.out.println(" ");
		System.out.println(" ");
	}

	public Carta getCartaPokerKicker() {
		return cartaPokerKicker;
	}

	public List<Carta> getCartePoker() {
		return cartePoker;
	}
	
	public Mano getMano() {
		return mano;
	}

	@Override
	public String toString() {
		return "Poker " + cartePoker;
	}
}
