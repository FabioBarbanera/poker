package implMano.controllaPunteggioMano;

import java.util.ArrayList;
import java.util.List;

import giocatore.mano.Carta;
import giocatore.mano.Mano;

public class ControllaPunteggioPoker {

	private Mano mano;
	private Carta cartaPokerKicker;
	
	public ControllaPunteggioPoker(Mano mano) {
		this.mano = mano;
	}

	public List<Carta> valuta() {

		// crtl+shift+B attivi e disattivi breakpoint sulla linea su cui ti trovi
		int contatoreUguaglianzaValori = 0;
		List<Carta> cartePoker = new ArrayList<Carta>();
		

		for (int i = 0; i < this.mano.getCarteDellaMano().length; i++) {
			for (int j = i + 1; j < this.mano.getCarteDellaMano().length; j++) {
				if (this.mano.getCarteDellaMano()[i].getValoreCarta().getValore() == this.mano.getCarteDellaMano()[j]
						.getValoreCarta().getValore()) {
					contatoreUguaglianzaValori++;
					cartePoker.add(this.mano.getCarteDellaMano()[i]);
					cartePoker.add(this.mano.getCarteDellaMano()[j]);
				}
			}
		}

		for (int i = 0; i < cartePoker.size(); i++) {
			for (int j = i + 1; j < cartePoker.size(); j++) {
				if (cartePoker.get(i) == cartePoker.get(j) && cartePoker.get(i) != null) {
					cartePoker.set(j, null);
				}
			}
		}

		for (int i = 0; i < cartePoker.size();) {
			if (cartePoker.get(i) == null) {
				cartePoker.remove(i);
			} else {
				i++;
			}
		}

		for (int i = 0; i < this.mano.getCarteDellaMano().length; i++) {
			if(cartePoker.size()!=0) {
				if (cartePoker.size()==4 && cartePoker.get(0).getValoreCarta().getValore() != 
					this.mano.getCarteDellaMano()[i].getValoreCarta().getValore()) {
					this.cartaPokerKicker=this.mano.getCarteDellaMano()[i];
				}
			}
		}

		if (contatoreUguaglianzaValori == 6 && cartePoker.size() == 4) {
			return cartePoker;
		}

		return null;

	}

	public Carta getCartaPokerKicker() {
		return this.cartaPokerKicker;
	}

}
