package giocatore;

import giocatore.mano.Mano;

public class Giocatore {

	private Mano manoGiocatore;
	private Integer indiceGiocatore;
	
	public Giocatore(Mano manoGiocatore, Integer indiceGiuocatore) {
		this.manoGiocatore=manoGiocatore;
		this.indiceGiocatore=indiceGiuocatore;
	}

	public Mano getManoGiocatore() {
		return manoGiocatore;
	}
	
	public Integer getIndiceGiocatore() {
		return indiceGiocatore;
	}
	
}
