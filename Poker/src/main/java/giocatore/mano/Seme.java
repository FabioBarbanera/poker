package giocatore.mano;

public class Seme {

	Character seme;
	
	public Seme(Character seme) {
		this.seme=seme;
	}

	@Override
	public String toString() {
		return "" + this.seme + "";
	}
	
	public Character getSeme() {
		return seme;
	}
}
