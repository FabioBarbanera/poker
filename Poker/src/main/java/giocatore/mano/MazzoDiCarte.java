package giocatore.mano;
import java.util.ArrayList;
import java.util.List;

public class MazzoDiCarte {

	private List<Carta> listaMazzoDiCarte = new ArrayList<Carta>();

	private MazzoDiCarte(List<Carta>listaMazzoDicarte) {
		this.listaMazzoDiCarte=listaMazzoDicarte;
	}
	
	public static MazzoDiCarte creaMazzo() {

		/*
		 * Creo una lista con tutti i valori sotto forma di stringa
		 * Ed una lista che conterra le carte del mazzo che mi servirà per creare il mazzo di carte
		 */
		List<Carta>listaMazzoDiCarte = new ArrayList<Carta>();
		
		List<String> caratteriValoriCarte = new ArrayList<String>();
		caratteriValoriCarte.add(" ");
		caratteriValoriCarte.add("2");
		caratteriValoriCarte.add("3");
		caratteriValoriCarte.add("4");
		caratteriValoriCarte.add("5");
		caratteriValoriCarte.add("6");
		caratteriValoriCarte.add("7");
		caratteriValoriCarte.add("8");
		caratteriValoriCarte.add("9");
		caratteriValoriCarte.add("10");
		caratteriValoriCarte.add("J");
		caratteriValoriCarte.add("Q");
		caratteriValoriCarte.add("K");
		caratteriValoriCarte.add("A");
		
		/*
		 * Vado ad aggiungere al mazzo le carte che creo 
		 * Per creare una carta ho bisogno di un carattere che la rappresenta e del suo valore
		 */
		
		/*
		 * CUORI
		 */
		for(int i=1; i<caratteriValoriCarte.size(); i++) {
			Carta carta = new Carta(new Seme('♥'), new ValoreCarta(caratteriValoriCarte.get(i), (i+1)));
			listaMazzoDiCarte.add(carta);
		}
		
		/*
		 * PICCHE
		 */
		for(int i=1; i<caratteriValoriCarte.size(); i++) {
			Carta carta = new Carta(new Seme('♠'), new ValoreCarta(caratteriValoriCarte.get(i), (i+1)));
			listaMazzoDiCarte.add(carta);
		}
		
		/*
		 * FIORI
		 */
		for(int i=1; i<caratteriValoriCarte.size(); i++) {
			Carta carta = new Carta(new Seme('♣'), new ValoreCarta(caratteriValoriCarte.get(i), (i+1)));
			listaMazzoDiCarte.add(carta);
		}
		
		/*
		 * QUADRI
		 */
		for(int i=1; i<caratteriValoriCarte.size(); i++) {
			Carta carta = new Carta(new Seme('♦'), new ValoreCarta(caratteriValoriCarte.get(i), (i+1)));
			listaMazzoDiCarte.add(carta);
		}
		
		return new MazzoDiCarte(listaMazzoDiCarte);
	}

	public List<Carta> getListaMazzoDiCarte() {
		return listaMazzoDiCarte;
	}
	
}
