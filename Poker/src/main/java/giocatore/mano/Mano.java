package giocatore.mano;
import java.util.Arrays;
import java.util.Collections;

public class Mano {


	Carta[]carteDellaMano = new Carta[5];
	
	public Mano(Carta[]carteDellaMano) {
		this.carteDellaMano=carteDellaMano;
	}
	
	public static Mano creaMano() {
		
		/*
		 * Creo il mazzo di carte
		 */
		MazzoDiCarte mazzoDiCarte = MazzoDiCarte.creaMazzo();
		
		/*
		 * Lo mischio
		 */
		Collections.shuffle(mazzoDiCarte.getListaMazzoDiCarte());
		
		/*
		 * Preparo un array che mi servirà per creare la Mano
		 */
		Carta[]carteDellaMano = new Carta[5];
		
		/*
		 * Riempio l'array con le prime cinque carte del mazzo mischiato. 
		 */
		for(int i=0; i<=4; i++) {//mentre faccio le prove mettere a 3 ma ricordarsi poi di rimetterlo a 4 
			carteDellaMano[i] = mazzoDiCarte.getListaMazzoDiCarte().get(i);
		}
		
		/*
		 * Per vedere Scala Reale dal 10
		 */
//		carteDellaMano[0] = mazzoDiCarte.getListaMazzoDiCarte().get(8);
//		carteDellaMano[1] = mazzoDiCarte.getListaMazzoDiCarte().get(9);
//		carteDellaMano[2] = mazzoDiCarte.getListaMazzoDiCarte().get(10);
//		carteDellaMano[3] = mazzoDiCarte.getListaMazzoDiCarte().get(11);
//		carteDellaMano[4] = mazzoDiCarte.getListaMazzoDiCarte().get(12);
		
		/*
		 * Per vedere Scala Reale con asso che vale 1
		 */
//		carteDellaMano[0] = mazzoDiCarte.getListaMazzoDiCarte().get(0);
//		carteDellaMano[1] = mazzoDiCarte.getListaMazzoDiCarte().get(1);
//		carteDellaMano[2] = mazzoDiCarte.getListaMazzoDiCarte().get(2);
//		carteDellaMano[3] = mazzoDiCarte.getListaMazzoDiCarte().get(3);
//		carteDellaMano[4] = mazzoDiCarte.getListaMazzoDiCarte().get(12);
		
		/*
		 * Per vedere scala con semi differenti
		 */
//		carteDellaMano[0] = mazzoDiCarte.getListaMazzoDiCarte().get(0);
//		carteDellaMano[1] = mazzoDiCarte.getListaMazzoDiCarte().get(14);
//		carteDellaMano[2] = mazzoDiCarte.getListaMazzoDiCarte().get(2);
//		carteDellaMano[3] = mazzoDiCarte.getListaMazzoDiCarte().get(3);
//		carteDellaMano[4] = mazzoDiCarte.getListaMazzoDiCarte().get(12);
		
//		carteDellaMano[0] = mazzoDiCarte.getListaMazzoDiCarte().get(10);
//		carteDellaMano[1] = mazzoDiCarte.getListaMazzoDiCarte().get(22);
//		carteDellaMano[2] = mazzoDiCarte.getListaMazzoDiCarte().get(8);
//		carteDellaMano[3] = mazzoDiCarte.getListaMazzoDiCarte().get(6);
//		carteDellaMano[4] = mazzoDiCarte.getListaMazzoDiCarte().get(20);
		
		/*
		 * Per vedere scala colore
		 */
//		carteDellaMano[0] = mazzoDiCarte.getListaMazzoDiCarte().get(2);
//		carteDellaMano[1] = mazzoDiCarte.getListaMazzoDiCarte().get(5);
//		carteDellaMano[2] = mazzoDiCarte.getListaMazzoDiCarte().get(3);
//		carteDellaMano[3] = mazzoDiCarte.getListaMazzoDiCarte().get(4);
//		carteDellaMano[4] = mazzoDiCarte.getListaMazzoDiCarte().get(1);
		
		/*
		 * Controllo Poker
		 */
//		carteDellaMano[0] = mazzoDiCarte.getListaMazzoDiCarte().get(17);
//		carteDellaMano[1] = mazzoDiCarte.getListaMazzoDiCarte().get(4);
//		carteDellaMano[2] = mazzoDiCarte.getListaMazzoDiCarte().get(43);
//		carteDellaMano[3] = mazzoDiCarte.getListaMazzoDiCarte().get(31);
//		carteDellaMano[4] = mazzoDiCarte.getListaMazzoDiCarte().get(30);

		/*
		 * Controllo Full
		 */
//		carteDellaMano[0] = mazzoDiCarte.getListaMazzoDiCarte().get(4);
//		carteDellaMano[1] = mazzoDiCarte.getListaMazzoDiCarte().get(17);
//		carteDellaMano[2] = mazzoDiCarte.getListaMazzoDiCarte().get(26);
//		carteDellaMano[3] = mazzoDiCarte.getListaMazzoDiCarte().get(30);
//		carteDellaMano[4] = mazzoDiCarte.getListaMazzoDiCarte().get(13);
//		
		/*
		 * ControlloColore
		 */
//		carteDellaMano[0] = mazzoDiCarte.getListaMazzoDiCarte().get(4);
//		carteDellaMano[1] = mazzoDiCarte.getListaMazzoDiCarte().get(6);
//		carteDellaMano[2] = mazzoDiCarte.getListaMazzoDiCarte().get(0);
//		carteDellaMano[3] = mazzoDiCarte.getListaMazzoDiCarte().get(8);
//		carteDellaMano[4] = mazzoDiCarte.getListaMazzoDiCarte().get(10);
	
		/*
		 * Controllo Tris
		 */
//		carteDellaMano[0] = mazzoDiCarte.getListaMazzoDiCarte().get(4);
//		carteDellaMano[1] = mazzoDiCarte.getListaMazzoDiCarte().get(17);
//		carteDellaMano[2] = mazzoDiCarte.getListaMazzoDiCarte().get(26);
//		carteDellaMano[3] = mazzoDiCarte.getListaMazzoDiCarte().get(30);
//		carteDellaMano[4] = mazzoDiCarte.getListaMazzoDiCarte().get(14);

		/*
		 * Controllo Doppia Coppia
		 */
//		carteDellaMano[0] = mazzoDiCarte.getListaMazzoDiCarte().get(4);
//		carteDellaMano[1] = mazzoDiCarte.getListaMazzoDiCarte().get(17);
//		carteDellaMano[2] = mazzoDiCarte.getListaMazzoDiCarte().get(26);
//		carteDellaMano[3] = mazzoDiCarte.getListaMazzoDiCarte().get(27);
//		carteDellaMano[4] = mazzoDiCarte.getListaMazzoDiCarte().get(14);
		
		/*
		 * Controllo Coppia
		 */
//		carteDellaMano[0] = mazzoDiCarte.getListaMazzoDiCarte().get(4);
//		carteDellaMano[1] = mazzoDiCarte.getListaMazzoDiCarte().get(18);
//		carteDellaMano[2] = mazzoDiCarte.getListaMazzoDiCarte().get(26);
//		carteDellaMano[3] = mazzoDiCarte.getListaMazzoDiCarte().get(39);
//		carteDellaMano[4] = mazzoDiCarte.getListaMazzoDiCarte().get(14);
		
		/*
		 * ControlloCartaPiuAlta
		 */
//		carteDellaMano[0] = mazzoDiCarte.getListaMazzoDiCarte().get(4);
//		carteDellaMano[1] = mazzoDiCarte.getListaMazzoDiCarte().get(18);
//		carteDellaMano[2] = mazzoDiCarte.getListaMazzoDiCarte().get(26);
//		carteDellaMano[3] = mazzoDiCarte.getListaMazzoDiCarte().get(42);
//		carteDellaMano[4] = mazzoDiCarte.getListaMazzoDiCarte().get(14);

		/*
		 * Contorllo Exception scala con mano cvincente carta piu alta
		 */
//		carteDellaMano[0] = mazzoDiCarte.getListaMazzoDiCarte().get(29);
//		carteDellaMano[1] = mazzoDiCarte.getListaMazzoDiCarte().get(43);
//		carteDellaMano[2] = mazzoDiCarte.getListaMazzoDiCarte().get(11);
//		carteDellaMano[3] = mazzoDiCarte.getListaMazzoDiCarte().get(45);
//		carteDellaMano[4] = mazzoDiCarte.getListaMazzoDiCarte().get(44);
		
		return new Mano(carteDellaMano);
	}
	
	public Carta[] getCarteDellaMano() {
		return this.carteDellaMano;
	}
	
	@Override
	public String toString() {
		return   Arrays.toString(carteDellaMano) ;
	}
}
