package giocatore.mano;

public class ValoreCarta {

	private String carattereValore;
	private Integer valore;
	
	public ValoreCarta(String carattereValore, Integer valore) {
		if(valore>14) {
			throw new NumberFormatException("Valore superiore a 13, il massimo valore di una carta");
		}
		this.carattereValore = carattereValore;
		this.valore = valore;
		
	}
	
	public String getCarattereValore() {
		return carattereValore;
	}
	public Integer getValore() {
		return valore;
	}

	public String toString() {
		return ""+this.carattereValore+"";
	}
	
	public void setValore(Integer valore) {
		this.valore = valore;
	}
}
