package giocatore.mano;

import utility.NumeroRandom;

public class Mazziere {

	private MazzoDiCarte mazzoDiCarte;
	
	public Mazziere(MazzoDiCarte mazzoDiCarte) {
		this.mazzoDiCarte = mazzoDiCarte;
	}
	
	/**
	 * questo metodo deve restituire una mano con numeri random ed azzerare le carte
	 * prese per non farle prendere di nuovo altra mano
	 * @return
	 */
	
	public Mano daiCarte(){
		
		Carta[]carteDellaMano = new Carta[5];
		
		for(int i=0; i<=4; i++) {
			Integer indiceCasuale = NumeroRandom.numeroRandomIndiceListaMazzoDiCarte();
			if(this.mazzoDiCarte.getListaMazzoDiCarte().get(indiceCasuale)!=null) {
				carteDellaMano[i] = this.mazzoDiCarte.getListaMazzoDiCarte().get(indiceCasuale);
				this.mazzoDiCarte.getListaMazzoDiCarte().set(indiceCasuale, null);
			}else {
				i--;
			}
		}
		
		/*
		 * Per prova poker
		 */
//		carteDellaMano[0] = mazzoDiCarte.getListaMazzoDiCarte().get(17);
//		carteDellaMano[1] = mazzoDiCarte.getListaMazzoDiCarte().get(4);
//		carteDellaMano[2] = mazzoDiCarte.getListaMazzoDiCarte().get(43);
//		carteDellaMano[3] = mazzoDiCarte.getListaMazzoDiCarte().get(31);
//		carteDellaMano[4] = mazzoDiCarte.getListaMazzoDiCarte().get(30);
		
		 return new Mano(carteDellaMano);
	}
}
