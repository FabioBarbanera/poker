package giocatore.mano;

public class Carta {

	Seme seme;
	ValoreCarta valoreCarta;
	
	public Carta(Seme seme, ValoreCarta valore) {
		this.seme = seme;
		this.valoreCarta = valore;
	}

	public Seme getSeme() {
		return seme;
	}

	public ValoreCarta getValoreCarta() {
		return valoreCarta;
	}

	@Override
	public String toString() {
		return valoreCarta.toString() + "" + seme.toString();
	}
	
	
}
