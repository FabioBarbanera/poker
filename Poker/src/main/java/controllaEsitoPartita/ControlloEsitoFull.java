package controllaEsitoPartita;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import giocatore.Giocatore;
import implMano.controllaPunteggioMano.risultatoMano.Full;

public class ControlloEsitoFull {

	private List<Object> possibiliCarteVincenti;
	private List<Giocatore> giocatori;
	private Giocatore giocatore;
	private Full fullPiuAlto;
	private Full fullSecondoPerConfronto;
	
	public ControlloEsitoFull(Map<List<Object>, List<Giocatore>>mappaCarteVincentiGiocatori) {
		
		Iterator <List<Object>> iteratore = mappaCarteVincentiGiocatori.keySet().iterator();
		this.possibiliCarteVincenti = iteratore.next();
		this.giocatori = mappaCarteVincentiGiocatori.get(this.possibiliCarteVincenti);
		
	}
	
	public Full valutaEsitoFullPiuAlto(int i) {
		
		if(this.possibiliCarteVincenti.get(i) instanceof Full) {
			/*
			 * Per controllare il full vincente:
			 * Pirma mi salvo in due variabili i due valori con cui si è fatto Full
			 * se il valore alla posizone 2 è = a quello alla poszione 4 
			 * allora le ultime tre sono il tris e le prime due la coppia
			 * Altrimenti se alla prima posizione e alla terza(posizione 2)
			 * ci sono valori uguali i primi tre posti sono il tris e gli utlimi due sono la coppia
			 */
			for(int k=1; k<=this.possibiliCarteVincenti.size(); k++) {
				
				if(this.fullPiuAlto == null) {
					this.fullPiuAlto = (Full)this.possibiliCarteVincenti.get(0);
				}
				
				if(this.giocatore == null) {
					this.giocatore = this.giocatori.get(0);
				}
				
				this.fullSecondoPerConfronto = (Full)this.possibiliCarteVincenti.get(k);
				
				Integer valoreTrisFullPiuAlto = null;
				Integer valoreTrisSecondoFull = null;
				Integer valoreCoppiaFullPiuAlto = null;
				Integer valoreCoppiaSecondoFull = null;
				Integer valoreCartaDiMezzoPrimaFullPiuAlto = this.fullPiuAlto.getMano().getCarteDellaMano()[2].getValoreCarta().getValore();
				Integer valoreCartaDiMezzoSecondaFullPiuAlto = this.fullPiuAlto.getMano().getCarteDellaMano()[3].getValoreCarta().getValore();;
				Integer valoreCartaInizialeFullPiuAlto = this.fullPiuAlto.getMano().getCarteDellaMano()[0].getValoreCarta().getValore();
				Integer valoreCartaFinaleFullPiuAlto = this.fullPiuAlto.getMano().getCarteDellaMano()[4].getValoreCarta().getValore();
				Integer valoreCartaDiMezzoPrimaFullSecondoConfronto = this.fullSecondoPerConfronto.getMano().getCarteDellaMano()[2].getValoreCarta().getValore();
				Integer valoreCartaDiMezzoSecondaFullSecondoConfronto = this.fullSecondoPerConfronto.getMano().getCarteDellaMano()[3].getValoreCarta().getValore();;
				Integer valoreCartaInizialeFullSecondoConfronto = this.fullSecondoPerConfronto.getMano().getCarteDellaMano()[0].getValoreCarta().getValore();
				Integer valoreCartaFinaleFullSecondoConfronto = this.fullSecondoPerConfronto.getMano().getCarteDellaMano()[4].getValoreCarta().getValore();
				
					if(valoreCartaDiMezzoPrimaFullPiuAlto == valoreCartaFinaleFullPiuAlto) {
						valoreTrisFullPiuAlto = valoreCartaFinaleFullPiuAlto;
						valoreCoppiaFullPiuAlto = valoreCartaInizialeFullPiuAlto;
					}else if(valoreCartaDiMezzoSecondaFullPiuAlto == valoreCartaInizialeFullPiuAlto) {
						valoreTrisFullPiuAlto = valoreCartaInizialeFullPiuAlto;
						valoreCoppiaFullPiuAlto = valoreCartaFinaleFullPiuAlto;
					}
					if(valoreCartaDiMezzoPrimaFullSecondoConfronto == valoreCartaFinaleFullSecondoConfronto) {
						valoreTrisSecondoFull = valoreCartaFinaleFullPiuAlto;
						valoreCoppiaSecondoFull = valoreCartaInizialeFullPiuAlto;
					}else if(valoreCartaDiMezzoSecondaFullSecondoConfronto == valoreCartaInizialeFullSecondoConfronto) {
						valoreTrisSecondoFull = valoreCartaInizialeFullPiuAlto;
						valoreCoppiaSecondoFull = valoreCartaFinaleFullPiuAlto;
					}
				
					if(valoreTrisFullPiuAlto < valoreTrisSecondoFull) {
						this.fullPiuAlto = this.fullSecondoPerConfronto;
						this.giocatore = this.giocatori.get(k);
						break;
					}else if(valoreTrisFullPiuAlto == valoreTrisSecondoFull) {
						if(valoreCoppiaFullPiuAlto > valoreCoppiaSecondoFull) {
							break;
						}else if(valoreCoppiaFullPiuAlto < valoreCoppiaSecondoFull) {
							this.fullPiuAlto = this.fullSecondoPerConfronto;
							this.giocatore = this.giocatori.get(k);
							break;
						}else if(valoreCoppiaFullPiuAlto == valoreCoppiaSecondoFull) {
							/*
							 * Patta
							 */
							System.out.println("Full Patto");
							break;
						}
					}
			}
			
		}
		return this.fullPiuAlto;
		
	}
	
	public Giocatore getGiocatore() {
		return giocatore;
	}
	
}
