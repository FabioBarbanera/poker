package controllaEsitoPartita;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import giocatore.Giocatore;
import implMano.controllaPunteggioMano.risultatoMano.Poker;

public class ControlloEsitoPoker {

	private List<Object> possibiliCarteVincenti;
	private List<Giocatore> giocatori;
	private Giocatore giocatore;
	private Poker pokerPiuAlto;
	private Poker pokerSecondoPerConfronto;
	
	public ControlloEsitoPoker(Map<List<Object>, List<Giocatore>>mappaCarteVincentiGiocatori) {
		
		Iterator <List<Object>> iteratore = mappaCarteVincentiGiocatori.keySet().iterator();
		this.possibiliCarteVincenti = iteratore.next();
		this.giocatori = mappaCarteVincentiGiocatori.get(this.possibiliCarteVincenti);
		
	}
	
	public Poker valutaEsitoPokerPiuAlto(int i) {
		
		if(possibiliCarteVincenti.get(i) instanceof Poker) {
			/*
			 * Prima mi controllo se, essendoci più mani del poker,
			 * uno dei pokjer è fatto con le carte più alte 
			 * Altrimenti verifico la carta che rimane se c'è una più alta nelle mani che hanno fatto poker
			 * altrimenti confronto i semi					
			 */
			for(int k=1; k <= this.possibiliCarteVincenti.size(); k++) {
				
				if(this.pokerPiuAlto == null) {
					this.pokerPiuAlto = (Poker)this.possibiliCarteVincenti.get(0);
				}
				
				if(this.giocatore == null) {
					this.giocatore = this.giocatori.get(0);
				}
				
				this.pokerSecondoPerConfronto = (Poker)this.possibiliCarteVincenti.get(k);
				
				Integer valorePokerPiuAlto = this.pokerPiuAlto.getCartePoker().get(0).getValoreCarta().getValore();
				Integer valorePokerSecondoPerConfronto = this.pokerSecondoPerConfronto.getCartePoker().get(0).getValoreCarta().getValore(); 
				Integer valoreCartaKickerPokerPiualto = this.pokerPiuAlto.getCartaPokerKicker().getValoreCarta().getValore();  
				Integer valoreCartaKickerPokerSecondoPerConfronto = this.pokerSecondoPerConfronto.getCartaPokerKicker().getValoreCarta().getValore();
				
				if(valorePokerPiuAlto < valorePokerSecondoPerConfronto) {
					this.pokerPiuAlto = this.pokerSecondoPerConfronto;
					this.giocatore = this.giocatori.get(k);
				}else if(valorePokerPiuAlto == valorePokerSecondoPerConfronto) {
					if(valoreCartaKickerPokerPiualto < valoreCartaKickerPokerSecondoPerConfronto) {
						this.pokerPiuAlto = this.pokerSecondoPerConfronto;
						this.giocatore = this.giocatori.get(k);
					}else if(valoreCartaKickerPokerPiualto == valoreCartaKickerPokerSecondoPerConfronto) {
						if(this.pokerPiuAlto.getCartaPokerKicker().getSeme().getSeme().equals('♥')) {
							break;
						}else if(this.pokerSecondoPerConfronto.getCartaPokerKicker().getSeme().getSeme().equals('♥')){
							this.pokerPiuAlto = this.pokerSecondoPerConfronto;
							this.giocatore = this.giocatori.get(k);
							break;
						}else if(this.pokerPiuAlto.getCartaPokerKicker().getSeme().getSeme().equals('♦')){
							break;
						}else if(this.pokerSecondoPerConfronto.getCartaPokerKicker().getSeme().getSeme().equals('♦')){
							this.pokerPiuAlto = this.pokerSecondoPerConfronto;
							this.giocatore = this.giocatori.get(k);
							break;
						}else if(this.pokerPiuAlto.getCartaPokerKicker().getSeme().getSeme().equals('♣')){
							break;
						}else if(this.pokerSecondoPerConfronto.getCartaPokerKicker().getSeme().getSeme().equals('♣')){
							this.pokerPiuAlto = this.pokerSecondoPerConfronto;
							this.giocatore = this.giocatori.get(k);
							break;
						}else if(this.pokerPiuAlto.getCartaPokerKicker().getSeme().getSeme().equals('♠')){
							break;
						}else if(this.pokerSecondoPerConfronto.getCartaPokerKicker().getSeme().getSeme().equals('♠')){
							this.pokerPiuAlto = this.pokerSecondoPerConfronto;
							this.giocatore = this.giocatori.get(k);
							break;
						}
					}
				}
			}	
		}
		return this.pokerPiuAlto;
		
	}
	
	public Giocatore getGiocatore() {
		return giocatore;
	}
	
}


