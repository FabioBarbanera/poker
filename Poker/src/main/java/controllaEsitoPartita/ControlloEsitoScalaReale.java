package controllaEsitoPartita;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import giocatore.Giocatore;
import implMano.controllaPunteggioMano.risultatoMano.ScalaReale;

public class ControlloEsitoScalaReale {

	private List<Object> possibiliCarteVincenti;
	private List<Giocatore> giocatori;
	private Giocatore giocatore;
	private ScalaReale scalaRealePiuAlta;
	private ScalaReale scalaRealeSecondaPerConfronto;
	
	public ControlloEsitoScalaReale(Map<List<Object>, List<Giocatore>>mappaCarteVincentiGiocatori) {
		
		Iterator <List<Object>> iteratore = mappaCarteVincentiGiocatori.keySet().iterator();
		this.possibiliCarteVincenti = iteratore.next();
		this.giocatori = mappaCarteVincentiGiocatori.get(this.possibiliCarteVincenti);
		
	}
	
	public ScalaReale valutaEsitoScalaRealePiuAlta(int i) {
		
		/*
		 * Primo controllo su scala reale:
		 * Vedo quale valore dell'utlima carta è maggiore tra le varie scale reali registrate
		 * (questo perche la scala è ordinata in maniera crescente, l'ultima carta sarà sicuramente la più alta della scala)
		 * se sono scale con l'ultima carta dello stesso valore seguo il principio dei semi
		 */
		
		if(this.possibiliCarteVincenti.get(i) instanceof ScalaReale) {
			
			for(int k=1; k <= this.possibiliCarteVincenti.size(); k++) {
				
				this.scalaRealeSecondaPerConfronto = (ScalaReale)this.possibiliCarteVincenti.get(k);
				
				if(this.scalaRealePiuAlta == null) {
					this.scalaRealePiuAlta = (ScalaReale)this.possibiliCarteVincenti.get(0);
				}
				
				if(this.giocatore == null) {
					this.giocatore = this.giocatori.get(0);
				}
				
				Integer valoreScalaRealePiuAlta = this.scalaRealePiuAlta.getMano().getCarteDellaMano()[4].getValoreCarta().getValore();
				Integer valoreScalaRealeSecondaPerConfronto = this.scalaRealeSecondaPerConfronto.getMano().getCarteDellaMano()[4].getValoreCarta().getValore(); 
				
				if(valoreScalaRealePiuAlta < valoreScalaRealeSecondaPerConfronto) {
					this.scalaRealePiuAlta = this.scalaRealeSecondaPerConfronto;
					this.giocatore = this.giocatori.get(k);
				}else if(valoreScalaRealePiuAlta == valoreScalaRealeSecondaPerConfronto) {
					if(scalaRealePiuAlta.getMano().getCarteDellaMano()[4].getSeme().getSeme().equals('♥')){
						break;
					}else if(this.scalaRealeSecondaPerConfronto.getMano().getCarteDellaMano()[4].getSeme().getSeme().equals('♥')){
						this.scalaRealePiuAlta = this.scalaRealeSecondaPerConfronto;
						this.giocatore = this.giocatori.get(k);
						break;
					}else if(scalaRealePiuAlta.getMano().getCarteDellaMano()[4].getSeme().getSeme().equals('♦')){
						break;
					}else if(scalaRealeSecondaPerConfronto.getMano().getCarteDellaMano()[4].getSeme().getSeme().equals('♦')){
						this.scalaRealePiuAlta = this.scalaRealeSecondaPerConfronto;
						this.giocatore = this.giocatori.get(k);
						break;
					}else if(this.scalaRealePiuAlta.getMano().getCarteDellaMano()[4].getSeme().getSeme().equals('♣')){
						break;
					}else if(this.scalaRealeSecondaPerConfronto.getMano().getCarteDellaMano()[4].getSeme().getSeme().equals('♣')){
						this.scalaRealePiuAlta = this.scalaRealeSecondaPerConfronto;
						this.giocatore = this.giocatori.get(k);
						break;
					}else if(this.scalaRealePiuAlta.getMano().getCarteDellaMano()[4].getSeme().getSeme().equals('♠')){
						break;
					}else if(this.scalaRealeSecondaPerConfronto.getMano().getCarteDellaMano()[4].getSeme().getSeme().equals('♠')){
						this.scalaRealePiuAlta = this.scalaRealeSecondaPerConfronto;
						this.giocatore = this.giocatori.get(k);
						break;
					}
				}
			}
		}
		return this.scalaRealePiuAlta;
		
	}
	
	public Giocatore getGiocatore() {
		return giocatore;
	}
	
}
