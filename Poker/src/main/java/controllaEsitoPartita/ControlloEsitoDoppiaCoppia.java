package controllaEsitoPartita;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import giocatore.Giocatore;
import implMano.controllaPunteggioMano.risultatoMano.DoppiaCoppia;

public class ControlloEsitoDoppiaCoppia {

	private List<Object> possibiliCarteVincenti;
	private List<Giocatore> giocatori;
	private Giocatore giocatore;
	private DoppiaCoppia doppiaCoppiaPiuAlta;
	private DoppiaCoppia doppiaCoppiaSecondaPerConfronto;
	
	public ControlloEsitoDoppiaCoppia(Map<List<Object>, List<Giocatore>>mappaCarteVincentiGiocatori) {
		
		Iterator <List<Object>> iteratore = mappaCarteVincentiGiocatori.keySet().iterator();
		this.possibiliCarteVincenti = iteratore.next();
		this.giocatori = mappaCarteVincentiGiocatori.get(this.possibiliCarteVincenti);
		
	}
		
	public DoppiaCoppia valutaEsitoTrisPiuAlto(int i) {
		
		if(this.possibiliCarteVincenti.get(i) instanceof DoppiaCoppia) {
			
			/*
			 * In caso di parità, vince quella con la seconda coppia più alta. 
			 * In caso di ulteriore pareggio, si divide il piatto oppure si va al confronto dei kicker. 
			 * Nella versione all'italiana, in caso di pareggio si segue la regola dei semi.
			 * Nel caso di pareggio del Kicker, Di quale carta va controllato il seme?
			 * In questo caso, non sapendolo, userò il seme del Kicker per definire il vincitore
			 */
			/*
			 * Quindi per controllare effettivamente la vincita prima confronto l'ultima posizione 
			 * del primo tris(la mano è stata ordinata in ordine crescente quindi sicuramente nella prima posizione ci sarà 
			 * il valore più basso delle coppie e all'ultimo quello più alto)
			 * poi nel caso fossero uguali confronto la prima posizione
			 * (dove sicuramente si troveranno i valori più bassi)
			 * in caso di ulteriore uguaglianza vado a confrontare il Kicker  
			 */
			
			for(int k=1; k<this.possibiliCarteVincenti.size();k++) {
				
				this.doppiaCoppiaSecondaPerConfronto = (DoppiaCoppia)this.possibiliCarteVincenti.get(k); 
				
				if(this.doppiaCoppiaPiuAlta == null){
					this.doppiaCoppiaPiuAlta = (DoppiaCoppia)this.possibiliCarteVincenti.get(0);
				}
				
				if(this.giocatore == null){
					this.giocatore = this.giocatori.get(0);
				}
				
				Integer valorePrimaCoppiaDoppiaCoppiaPiuAlta = this.doppiaCoppiaPiuAlta.getCarteDoppiaCoppia().get(3).getValoreCarta().getValore();
				Integer valoreSecondaCoppiaDoppiaCoppiaPiuAlta = this.doppiaCoppiaPiuAlta.getCarteDoppiaCoppia().get(0).getValoreCarta().getValore();
				Integer valorePrimaCoppiaDoppiaCoppiaPerConfronto = this.doppiaCoppiaSecondaPerConfronto.getCarteDoppiaCoppia().get(3).getValoreCarta().getValore();
				Integer valoreSecondaCoppiaDoppiaCoppiaPerConfronto = this.doppiaCoppiaSecondaPerConfronto.getCarteDoppiaCoppia().get(0).getValoreCarta().getValore();
				
				if(valorePrimaCoppiaDoppiaCoppiaPiuAlta < valorePrimaCoppiaDoppiaCoppiaPerConfronto) {
					this.doppiaCoppiaPiuAlta = this.doppiaCoppiaSecondaPerConfronto;
					this.giocatore = this.giocatori.get(k);
				}else if(valorePrimaCoppiaDoppiaCoppiaPiuAlta == valorePrimaCoppiaDoppiaCoppiaPerConfronto) {
					if(valoreSecondaCoppiaDoppiaCoppiaPiuAlta < valoreSecondaCoppiaDoppiaCoppiaPerConfronto) {
						this.doppiaCoppiaPiuAlta = this.doppiaCoppiaSecondaPerConfronto;
						this.giocatore = this.giocatori.get(k);
					}else if(valoreSecondaCoppiaDoppiaCoppiaPiuAlta == valoreSecondaCoppiaDoppiaCoppiaPerConfronto) {
						/*
						 * prendere carta Kicker
						 */
						Integer valoreCartaKickerDoppiaCoppiaPiuAlta = this.doppiaCoppiaPiuAlta.getCartaKicker().getValoreCarta().getValore();
						Integer valoreCartaKickerDoppiaCoppiaSecondaPerConfronto = this.doppiaCoppiaSecondaPerConfronto.getCartaKicker().getValoreCarta().getValore();
						
						if(valoreCartaKickerDoppiaCoppiaPiuAlta > valoreCartaKickerDoppiaCoppiaSecondaPerConfronto) {
							break;
						}else if(valoreCartaKickerDoppiaCoppiaPiuAlta < valoreCartaKickerDoppiaCoppiaSecondaPerConfronto) {
							this.doppiaCoppiaPiuAlta = this.doppiaCoppiaSecondaPerConfronto;
							this.giocatore = this.giocatori.get(k);
							break;
						}else if(valoreCartaKickerDoppiaCoppiaPiuAlta == valoreCartaKickerDoppiaCoppiaSecondaPerConfronto) {
							/*
							 * Capire se effettivamente devono andare a confrontare il seme del kicker o di qualche coppia o pattarla
							 */
							System.out.println("Doppia coppia patta");
							break;
						}
					} 
				}
			}
			
		}
		return this.doppiaCoppiaPiuAlta;
		
	}
	
	public Giocatore getGiocatore() {
		return this.giocatore;
	}
	
}
