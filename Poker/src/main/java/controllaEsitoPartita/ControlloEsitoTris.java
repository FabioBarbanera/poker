package controllaEsitoPartita;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import giocatore.Giocatore;
import implMano.controllaPunteggioMano.risultatoMano.Tris;

public class ControlloEsitoTris {

	private List<Object> possibiliCarteVincenti;
	private List<Giocatore> giocatori;
	private Giocatore giocatore;
	private Tris trisPiuAlto;
	private Tris trisSecondoPerConfronto;
	
	public ControlloEsitoTris(Map<List<Object>, List<Giocatore>>mappaCarteVincentiGiocatori) {
		
		Iterator <List<Object>> iteratore = mappaCarteVincentiGiocatori.keySet().iterator();
		this.possibiliCarteVincenti = iteratore.next();
		this.giocatori = mappaCarteVincentiGiocatori.get(this.possibiliCarteVincenti);
		
	}
	
	public Tris valutaEsitoTrisPiuAlto(int i) {
		
		if(this.possibiliCarteVincenti.get(i) instanceof Tris) {
			
			/*
			 * In caso di due o più tris, vince quello di valore maggiore. 
			 * In caso di parità, si divide il piatto oppure si va al confronto dei kicker e, 
			 * nel caso di ulteriore pareggio, del colore della carta più alta.
			 */
			
			for(int k=0; k<this.possibiliCarteVincenti.size();k++) {
			
				this.trisSecondoPerConfronto = (Tris)this.possibiliCarteVincenti.get(k);
				/*
			 * Prima controllo il valore della carta del tris delle due mani
			 * vince quella con il tris maggiore(imposto già la variabile tris piu alto con il valore di default della prima della lista)
			 * 
			 */
				if(trisPiuAlto == null) {
					this.trisPiuAlto = (Tris)this.possibiliCarteVincenti.get(0);
				}
				if(this.giocatore==null) {
					this.giocatore = this.giocatori.get(0);
				}
				
				Integer valoreCartaTrisPiuAlto = this.trisPiuAlto.getCarteTris().get(0).getValoreCarta().getValore();
				Integer valoreCartaTrisSecondoPerConfronto = this.trisSecondoPerConfronto.getCarteTris().get(0).getValoreCarta().getValore();
				
				if(valoreCartaTrisPiuAlto < valoreCartaTrisSecondoPerConfronto) {
					this.trisPiuAlto = this.trisSecondoPerConfronto;
					this.giocatore = this.giocatori.get(k);
					/*
					 * Poi se il valore dei tris è uguale vado a controllare il valore delle carte kicker 
					 */
				}else if(valoreCartaTrisPiuAlto == valoreCartaTrisSecondoPerConfronto) {
					
					for(int x=1; x>=0 ;x--) {
							
							Integer valoreCartaKickerTrisPiuAlto = this.trisPiuAlto.getCarteKicker().get(x).getValoreCarta().getValore();
							Integer valoreCartaKickerTrisSecondoPerConfronto = this.trisSecondoPerConfronto.getCarteKicker().get(x).getValoreCarta().getValore();
							
							if(valoreCartaKickerTrisPiuAlto > valoreCartaKickerTrisSecondoPerConfronto) {
								break;
							}else if(valoreCartaKickerTrisPiuAlto < valoreCartaKickerTrisSecondoPerConfronto) {
								this.trisPiuAlto = this.trisSecondoPerConfronto;
								this.giocatore = this.giocatori.get(k);
								break;
							}else if(valoreCartaKickerTrisPiuAlto == valoreCartaKickerTrisSecondoPerConfronto && x==0) {
								/*
								 * Se anche il valore delle carte kicker fosse uguale confronto i semi
								 */
								if(this.trisPiuAlto.getCarteKicker().get(x).getSeme().getSeme().equals('♥')) {
									break;
								}else if(trisSecondoPerConfronto.getCarteKicker().get(x).getSeme().getSeme().equals('♥')) {
									this.giocatore = this.giocatori.get(k);
									this.trisPiuAlto = this.trisSecondoPerConfronto; 
									break;
								}else if(this.trisPiuAlto.getCarteKicker().get(x).getSeme().getSeme().equals('♦')) {
									break;
								}else if(trisSecondoPerConfronto.getCarteKicker().get(x).getSeme().getSeme().equals('♦')) {
									this.giocatore = this.giocatori.get(k);
									this.trisPiuAlto = this.trisSecondoPerConfronto; 
									break;
								}else if(this.trisPiuAlto.getCarteKicker().get(x).getSeme().getSeme().equals('♣')) {
									break;
								}else if(trisSecondoPerConfronto.getCarteKicker().get(x).getSeme().getSeme().equals('♣')) {
									this.giocatore = this.giocatori.get(k);
									this.trisPiuAlto = this.trisSecondoPerConfronto; 
									break;
								}else if(trisPiuAlto.getCarteKicker().get(x).getSeme().getSeme().equals('♠')) {
									break;
								}else if(trisSecondoPerConfronto.getCarteKicker().get(x).getSeme().getSeme().equals('♠')) {
									this.giocatore = this.giocatori.get(k);
									this.trisPiuAlto = this.trisSecondoPerConfronto; 
									break;
								}
							}
						}
					}
			}
		}
		return this.trisPiuAlto;
	}
	
	public Giocatore getGiocatore() {
		return giocatore;
	}
	
}
