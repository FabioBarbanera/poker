package controllaEsitoPartita;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import giocatore.Giocatore;
import implMano.controllaPunteggioMano.risultatoMano.Scala;

public class ControlloEsitoScala {

	private List<Object> possibiliCarteVincenti;
	private List<Giocatore> giocatori;
	private Giocatore giocatore;
	private Scala scalaSemplicePiuAlta;
	private Scala scalaSempliceSecondaPerConfronto;
	
	public ControlloEsitoScala(Map<List<Object>, List<Giocatore>>mappaCarteVincentiGiocatori) {
		
		Iterator <List<Object>> iteratore = mappaCarteVincentiGiocatori.keySet().iterator();
		this.possibiliCarteVincenti = iteratore.next();
		this.giocatori = mappaCarteVincentiGiocatori.get(this.possibiliCarteVincenti);
		
	}
	
	public Scala valutaEsitoScalaPiuAlta(int i) {
		
		if(possibiliCarteVincenti.get(i) instanceof Scala) {
			/*
			 * In caso di parità di scala la carta più piccola viene considerata come Kicker 
			 * Quindi si usa la regola dei semi per valutare il vincitore in caso di parità dei valori
			 */
			for(int k=1; k<=this.possibiliCarteVincenti.size(); k++) {
				
				if(this.scalaSemplicePiuAlta == null) {
					this.scalaSemplicePiuAlta = (Scala)this.possibiliCarteVincenti.get(0);
				}
				
				this.scalaSempliceSecondaPerConfronto = (Scala)this.possibiliCarteVincenti.get(k);
				
				Integer valoreScalaSemplicePiuAlta =  this.scalaSemplicePiuAlta.getMano().getCarteDellaMano()[4].getValoreCarta().getValore();
				Integer valoreScalaSempliceSecondaPerConfronto = this.scalaSempliceSecondaPerConfronto.getMano().getCarteDellaMano()[4].getValoreCarta().getValore();
				
				if(valoreScalaSemplicePiuAlta < valoreScalaSempliceSecondaPerConfronto) {
					this.scalaSemplicePiuAlta = this.scalaSempliceSecondaPerConfronto;
				}else if(valoreScalaSemplicePiuAlta == valoreScalaSempliceSecondaPerConfronto) {
					if(this.scalaSemplicePiuAlta.getMano().getCarteDellaMano()[0].getSeme().getSeme().equals('♥')) {
						break;
					}else if(this.scalaSempliceSecondaPerConfronto.getMano().getCarteDellaMano()[0].getSeme().getSeme().equals('♥')){
						this.scalaSemplicePiuAlta = this.scalaSempliceSecondaPerConfronto;
						this.giocatore = this.giocatori.get(k);
						break;
					}else if(this.scalaSemplicePiuAlta.getMano().getCarteDellaMano()[0].getSeme().getSeme().equals('♦')){
						break;
					}else if(this.scalaSempliceSecondaPerConfronto.getMano().getCarteDellaMano()[0].getSeme().getSeme().equals('♦')){
						this.scalaSemplicePiuAlta = this.scalaSempliceSecondaPerConfronto;
						this.giocatore = this.giocatori.get(k);
						break;
					}else if(this.scalaSemplicePiuAlta.getMano().getCarteDellaMano()[0].getSeme().getSeme().equals('♣')){
						break;
					}else if(this.scalaSempliceSecondaPerConfronto.getMano().getCarteDellaMano()[0].getSeme().getSeme().equals('♣')){
						this.scalaSemplicePiuAlta = this.scalaSempliceSecondaPerConfronto;
						this.giocatore = this.giocatori.get(k);
						break;
					}else if(this.scalaSemplicePiuAlta.getMano().getCarteDellaMano()[0].getSeme().getSeme().equals('♠')){
						break;
					}else if(this.scalaSempliceSecondaPerConfronto.getMano().getCarteDellaMano()[0].getSeme().getSeme().equals('♠')){
						this.scalaSemplicePiuAlta = this.scalaSempliceSecondaPerConfronto;
						this.giocatore = this.giocatori.get(k);
						break;
					}
				}
			}
		}
		return this.scalaSemplicePiuAlta;
		
	}

	public Giocatore getGiocatore() {
		return giocatore;
	}
	
}
