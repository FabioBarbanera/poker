package controllaEsitoPartita;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import giocatore.Giocatore;
import implMano.controllaPunteggioMano.risultatoMano.CartaPiuAlta;

public class ControlloEsitoCartaAlta {

	private List<Object> possibiliCarteVincenti;
	private List<Giocatore> giocatori;
	private Giocatore giocatore;
	private CartaPiuAlta cartaPiuAlta;
	private CartaPiuAlta cartaAltaSecondaPerConfronto;

	public ControlloEsitoCartaAlta(Map<List<Object>, List<Giocatore>>mappaCarteVincentiGiocatori) {
		
		Iterator <List<Object>> iteratore = mappaCarteVincentiGiocatori.keySet().iterator();
		this.possibiliCarteVincenti = iteratore.next();
		this.giocatori = mappaCarteVincentiGiocatori.get(this.possibiliCarteVincenti);
		
	}
	
	public CartaPiuAlta valutaEsitoCartaAlta(int i) {

		if(this.possibiliCarteVincenti.get(i) instanceof CartaPiuAlta) {
			
			for(int k=1; k<this.possibiliCarteVincenti.size();k++) {

				/*
				 * Al primo giro cartaAltasecondaperconfronto sarà alla posizione 1 perché k parte da 1 
				 */
				this.cartaAltaSecondaPerConfronto = (CartaPiuAlta)this.possibiliCarteVincenti.get(k);
				
				/*
				 * Mentre carta più alta sarà alla poszione zero perché se è nulla la inizializzo con il valore della prima posizione
				 */
				if(this.cartaPiuAlta == null) {
					this.cartaPiuAlta=(CartaPiuAlta)this.possibiliCarteVincenti.get(0);
				}
				
				/*
				 * La stessa cosa faccio con il giocatore
				 */
				if(this.giocatore==null) {
					this.giocatore = this.giocatori.get(0);
				}
				
				Integer valoreCartaAlta = this.cartaPiuAlta.getCartaAlta().getValoreCarta().getValore();
				Integer valoreCartaAltaSecondaPerConfronto = this.cartaAltaSecondaPerConfronto.getCartaAlta().getValoreCarta().getValore(); 
				
				if(valoreCartaAlta < valoreCartaAltaSecondaPerConfronto) {
					this.cartaPiuAlta = this.cartaAltaSecondaPerConfronto;
					this.giocatore = this.giocatori.get(k);
				}else if(valoreCartaAlta == valoreCartaAltaSecondaPerConfronto) {
					
					for(int x=3; x>=0; x--) {
						Integer valoreCartakickerPrima = this.cartaPiuAlta.getMano().getCarteDellaMano()[x].getValoreCarta().getValore();
						Integer valoreCartaKickerSeconda = this.cartaAltaSecondaPerConfronto.getMano().getCarteDellaMano()[x].getValoreCarta().getValore();
						/*
						 * Se il valore delle carte di questo giro del ciclo for con la i 
						 * sono uguali
						 */
						if(valoreCartakickerPrima > valoreCartaKickerSeconda) {
							/*
							 * La posizione rimane quella del giocatore impostata perché ovviamente
							 * la carta più altas non è cambiata. 
							 */
							break;
						}
						else if(valoreCartakickerPrima < valoreCartaKickerSeconda){
							/*
							 * Altrimenti se cambia la carta cambia anche la posizione del giocatore
							 */
							this.cartaPiuAlta = this.cartaAltaSecondaPerConfronto;
								this.giocatore = this.giocatori.get(k);
								break;
						}else if(valoreCartakickerPrima == valoreCartaKickerSeconda) {
							
							/*
							 * se i è all'ultima posizione dell'array e comunque è uguale significa che tutte le carte 
							 * sono uguali e quindi è patta
							 */
							if(x==0 && i==this.possibiliCarteVincenti.size()-1) {
								System.out.println("Carta Alta Patta");
								break;
							}
						}
					}
				}
			}
		}
			
		return this.cartaPiuAlta;
		
	}

	public Giocatore getGiocatore() {
		return this.giocatore;
	}
	
	
}
