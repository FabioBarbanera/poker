package controllaEsitoPartita;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import giocatore.Giocatore;
import implMano.controllaPunteggioMano.risultatoMano.ScalaColore;

public class ControlloEsitoScalaColore {

	private List<Object> possibiliCarteVincenti;
	private List<Giocatore> giocatori;
	private Giocatore giocatore;
	private ScalaColore scalaColorePiuAlta;
	private ScalaColore scalaColoreSecondaPerConfronto;
	
	public ControlloEsitoScalaColore(Map<List<Object>, List<Giocatore>>mappaCarteVincentiGiocatori) {
		
		Iterator <List<Object>> iteratore = mappaCarteVincentiGiocatori.keySet().iterator();
		this.possibiliCarteVincenti = iteratore.next();
		this.giocatori = mappaCarteVincentiGiocatori.get(this.possibiliCarteVincenti);
		
	}
	
	public ScalaColore valutaEsitoScalaColorePiuAlta(int i) {
		
		if(this.possibiliCarteVincenti.get(i) instanceof ScalaColore) {

			for(int k=1; k <= this.possibiliCarteVincenti.size(); k++) {
				
				this.scalaColoreSecondaPerConfronto = (ScalaColore)this.possibiliCarteVincenti.get(k);
				
				if(this.scalaColorePiuAlta == null) {
					this.scalaColorePiuAlta = (ScalaColore)this.possibiliCarteVincenti.get(0);
				}
				if(this.giocatore == null) {
					this.giocatore = this.giocatori.get(0);
				}
				
				Integer valoreCartaScalaColorePiuAlta = this.scalaColorePiuAlta.getMano().getCarteDellaMano()[4].getValoreCarta().getValore();
				Integer valoreCartaScalaColoreSecondaPerConfronto = this.scalaColoreSecondaPerConfronto.getMano().getCarteDellaMano()[4].getValoreCarta().getValore(); 
				
				if(valoreCartaScalaColorePiuAlta < valoreCartaScalaColoreSecondaPerConfronto) {
					this.scalaColorePiuAlta = this.scalaColoreSecondaPerConfronto;
					this.giocatore = this.giocatori.get(k);
				}else if(valoreCartaScalaColorePiuAlta < valoreCartaScalaColoreSecondaPerConfronto) {
					if(this.scalaColorePiuAlta.getMano().getCarteDellaMano()[4].getSeme().getSeme().equals('♥')) {
						break;
					}else if(this.scalaColoreSecondaPerConfronto.getMano().getCarteDellaMano()[4].getSeme().getSeme().equals('♥')){
						this.scalaColorePiuAlta = this.scalaColoreSecondaPerConfronto;
						this.giocatore = this.giocatori.get(k);
						break;
					}else if(this.scalaColorePiuAlta.getMano().getCarteDellaMano()[4].getSeme().getSeme().equals('♦')){
						break;
					}else if(scalaColoreSecondaPerConfronto.getMano().getCarteDellaMano()[4].getSeme().getSeme().equals('♦')){
						this.scalaColorePiuAlta = this.scalaColoreSecondaPerConfronto;
						this.giocatore = this.giocatori.get(k);
						break;
					}else if(this.scalaColorePiuAlta.getMano().getCarteDellaMano()[4].getSeme().getSeme().equals('♣')){
						break;
					}else if(this.scalaColoreSecondaPerConfronto.getMano().getCarteDellaMano()[4].getSeme().getSeme().equals('♣')){
						this.scalaColorePiuAlta = this.scalaColoreSecondaPerConfronto;
						this.giocatore = this.giocatori.get(k);
						break;
					}else if(this.scalaColorePiuAlta.getMano().getCarteDellaMano()[4].getSeme().getSeme().equals('♠')){
						break;
					}else if(this.scalaColoreSecondaPerConfronto.getMano().getCarteDellaMano()[4].getSeme().getSeme().equals('♠')){
						this.scalaColorePiuAlta = this.scalaColoreSecondaPerConfronto;this.giocatore = this.giocatori.get(k);
						break;
					}
				}
			}
		}
		return this.scalaColorePiuAlta;

		
	}
	
	public Giocatore getGiocatore() {
		return giocatore;
	}
	
}
