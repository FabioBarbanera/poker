package controllaEsitoPartita;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import giocatore.Giocatore;
import implMano.controllaPunteggioMano.risultatoMano.Coppia;

public class ControlloEsitoCoppia {

	private List<Object> possibiliCarteVincenti;
	private List<Giocatore> giocatori;
	private Giocatore giocatore;
	private Coppia coppiaPiuAlta;
	private Coppia coppiaSecondaPerIlConfronto;
	
	public ControlloEsitoCoppia(Map<List<Object>, List<Giocatore>>mappaCarteVincentiGiocatori) {
		
		Iterator <List<Object>> iteratore = mappaCarteVincentiGiocatori.keySet().iterator();
		this.possibiliCarteVincenti = iteratore.next();
		this.giocatori = mappaCarteVincentiGiocatori.get(this.possibiliCarteVincenti);
		
	}
	
	public Coppia valutaEsitoCoppiAlta(int i) {
			
			if(possibiliCarteVincenti.get(i) instanceof Coppia) {
			
				for(int k=1; k<this.possibiliCarteVincenti.size();k++) {
					this.coppiaSecondaPerIlConfronto = (Coppia)this.possibiliCarteVincenti.get(k);
					
					if(this.coppiaPiuAlta==null) {
						this.coppiaPiuAlta=(Coppia)this.possibiliCarteVincenti.get(0);
					}
					
					if(this.giocatore==null) {
						this.giocatore = this.giocatori.get(0);
					}
					
					Integer valoreCartaCoppiaPiuAlta = this.coppiaPiuAlta.getCarteCoppia().get(0).getValoreCarta().getValore();
					Integer valoreCartaCoppiaSecondaPerConfronto = this.coppiaSecondaPerIlConfronto.getCarteCoppia().get(0).getValoreCarta().getValore();
					
					if(valoreCartaCoppiaPiuAlta < valoreCartaCoppiaSecondaPerConfronto) {
						this.coppiaPiuAlta = this.coppiaSecondaPerIlConfronto;
						this.giocatore = this.giocatori.get(k);
					}else if(valoreCartaCoppiaPiuAlta == valoreCartaCoppiaSecondaPerConfronto && k!=0) {
						/*
						 * In questo caso inzio a confrontare i valori delle carte Kicker
						 */
						
							for(int x=2; x>=0; x--) {
								Integer valoreCartaKickerCoppiaPiuAlta = this.coppiaPiuAlta.getCarteKikcer().get(x).getValoreCarta().getValore();
								Integer valoreCartaKickerCoppiaSecondaPerConfronto = this.coppiaSecondaPerIlConfronto.getCarteKikcer().get(x).getValoreCarta().getValore();
								
								if(valoreCartaKickerCoppiaPiuAlta > valoreCartaKickerCoppiaSecondaPerConfronto) {
									break;
								}else if(valoreCartaKickerCoppiaPiuAlta < valoreCartaKickerCoppiaSecondaPerConfronto) {
									this.coppiaPiuAlta = this.coppiaSecondaPerIlConfronto;
									this.giocatore = this.giocatori.get(k);
									break;
								}else if(valoreCartaKickerCoppiaPiuAlta == valoreCartaKickerCoppiaSecondaPerConfronto) {
									if(x==0 && i == this.possibiliCarteVincenti.size()-1) {
										/*
										 * Significa che anche il valore dell'ultima carta è ugale
										 * Capire se darla patta oppure seguire la regola dei semi
										 * Ma capire a queli carta o carte attribuire tale regola
										 */
										System.out.println("Coppia patta");
										break;
									}
								}
							}
						}
					}
				}	
		return this.coppiaPiuAlta;
	}
	
	public Giocatore getGiocatore() {
		return this.giocatore;
	}

	
}
