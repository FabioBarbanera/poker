package controllaEsitoPartita.esito;

public class EsitoManoGiocatore {

	private Object tipoDiVincita;
	
	public EsitoManoGiocatore(Object tipoDiVincita) {
		this.tipoDiVincita = tipoDiVincita;
	}

	public Object getTipoDiVincita() {
		return tipoDiVincita;
	}

	@Override
	public String toString() {
		return "EsitoManoGiocatore " + tipoDiVincita;
	}
}
