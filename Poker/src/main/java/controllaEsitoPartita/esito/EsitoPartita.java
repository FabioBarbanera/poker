package controllaEsitoPartita.esito;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import controllaEsitoPartita.ControlloEsitoCartaAlta;
import controllaEsitoPartita.ControlloEsitoColore;
import controllaEsitoPartita.ControlloEsitoCoppia;
import controllaEsitoPartita.ControlloEsitoDoppiaCoppia;
import controllaEsitoPartita.ControlloEsitoFull;
import controllaEsitoPartita.ControlloEsitoPoker;
import controllaEsitoPartita.ControlloEsitoScala;
import controllaEsitoPartita.ControlloEsitoScalaColore;
import controllaEsitoPartita.ControlloEsitoScalaReale;
import controllaEsitoPartita.ControlloEsitoTris;
import giocatore.Giocatore;
import implMano.controllaPunteggioMano.risultatoMano.CartaPiuAlta;
import implMano.controllaPunteggioMano.risultatoMano.Colore;
import implMano.controllaPunteggioMano.risultatoMano.Coppia;
import implMano.controllaPunteggioMano.risultatoMano.DoppiaCoppia;
import implMano.controllaPunteggioMano.risultatoMano.Full;
import implMano.controllaPunteggioMano.risultatoMano.Poker;
import implMano.controllaPunteggioMano.risultatoMano.Scala;
import implMano.controllaPunteggioMano.risultatoMano.ScalaColore;
import implMano.controllaPunteggioMano.risultatoMano.ScalaReale;
import implMano.controllaPunteggioMano.risultatoMano.Tris;

/**
 * poi valutare se fare una classe vincitore +
 * a cui viene passata il tipo di vincita, il giocatore vincitore e la relativa mano.
 */

public class EsitoPartita {

	private List<EsitoManoGiocatore> listaEsitiMani;
	private List<Giocatore> listaGiocatori;

	public EsitoPartita(List<EsitoManoGiocatore> listaEsitiMani, List<Giocatore> listaGiocatori) {
		this.listaEsitiMani = listaEsitiMani;
		this.listaGiocatori = listaGiocatori;
	}

	public void valuta() {

		List<Map<Giocatore, EsitoManoGiocatore>> listaMappe = new ArrayList<Map<Giocatore, EsitoManoGiocatore>>();

		Map<Giocatore, EsitoManoGiocatore> mappaScaleReali = new HashMap<Giocatore, EsitoManoGiocatore>();
		Map<Giocatore, EsitoManoGiocatore> mappaScaleColori = new HashMap<Giocatore, EsitoManoGiocatore>();
		Map<Giocatore, EsitoManoGiocatore> mappaPoker = new HashMap<Giocatore, EsitoManoGiocatore>();
		Map<Giocatore, EsitoManoGiocatore> mappaFull = new HashMap<Giocatore, EsitoManoGiocatore>();
		Map<Giocatore, EsitoManoGiocatore> mappaColore = new HashMap<Giocatore, EsitoManoGiocatore>();
		Map<Giocatore, EsitoManoGiocatore> mappaScaleSemplici = new HashMap<Giocatore, EsitoManoGiocatore>();
		Map<Giocatore, EsitoManoGiocatore> mappaTris = new HashMap<Giocatore, EsitoManoGiocatore>();
		Map<Giocatore, EsitoManoGiocatore> mappaDoppieCoppie = new HashMap<Giocatore, EsitoManoGiocatore>();
		Map<Giocatore, EsitoManoGiocatore> mappaCoppie = new HashMap<Giocatore, EsitoManoGiocatore>();
		Map<Giocatore, EsitoManoGiocatore> mappaCarteAlte = new HashMap<Giocatore, EsitoManoGiocatore>();
		
		List<Object> possibiliCarteVincenti = new ArrayList<Object>();
		List<Giocatore> possibiliGiocatoriVincenti = new ArrayList<Giocatore>();
		Map<List<Object>, List<Giocatore>> mappaCarteVincentiGiocatori = new HashMap<List<Object>, List<Giocatore>>();

		/*
		 * In base alla tipologia di istanza aggiungo alla relativa mappa la mano:
		 * Il giocatore e la sua mano di quel tipo di istanza.
		 */
		
		for (int i = 0; i < this.listaEsitiMani.size(); i++) {

			if (this.listaEsitiMani.get(i).getTipoDiVincita() instanceof ScalaReale) {
				mappaScaleReali.put(this.listaGiocatori.get(i), this.listaEsitiMani.get(i));
			} else if (this.listaEsitiMani.get(i).getTipoDiVincita() instanceof ScalaColore) {
				mappaScaleColori.put(this.listaGiocatori.get(i), this.listaEsitiMani.get(i));
			}else if (this.listaEsitiMani.get(i).getTipoDiVincita() instanceof Poker) {
				mappaPoker.put(this.listaGiocatori.get(i), this.listaEsitiMani.get(i));
			} else if (this.listaEsitiMani.get(i).getTipoDiVincita() instanceof Full) {
				mappaFull.put(this.listaGiocatori.get(i), this.listaEsitiMani.get(i));
			} else if (this.listaEsitiMani.get(i).getTipoDiVincita() instanceof Colore) {
				mappaColore.put(this.listaGiocatori.get(i), this.listaEsitiMani.get(i));
			} else if (this.listaEsitiMani.get(i).getTipoDiVincita() instanceof Scala) {
				mappaScaleSemplici.put(this.listaGiocatori.get(i), this.listaEsitiMani.get(i));
			} else if (this.listaEsitiMani.get(i).getTipoDiVincita() instanceof Tris) {
				mappaTris.put(this.listaGiocatori.get(i), this.listaEsitiMani.get(i));
			} else if (this.listaEsitiMani.get(i).getTipoDiVincita() instanceof DoppiaCoppia) {
				mappaDoppieCoppie.put(this.listaGiocatori.get(i), this.listaEsitiMani.get(i));
			} else if (this.listaEsitiMani.get(i).getTipoDiVincita() instanceof Coppia) {
				mappaCoppie.put(this.listaGiocatori.get(i), this.listaEsitiMani.get(i));
			} else if (this.listaEsitiMani.get(i).getTipoDiVincita() instanceof CartaPiuAlta) {
				mappaCarteAlte.put(this.listaGiocatori.get(i), this.listaEsitiMani.get(i));
			}
		}

		listaMappe.add(mappaScaleReali);
		listaMappe.add(mappaScaleColori);
		listaMappe.add(mappaPoker);
		listaMappe.add(mappaFull);
		listaMappe.add(mappaColore);
		listaMappe.add(mappaScaleSemplici);
		listaMappe.add(mappaTris);
		listaMappe.add(mappaDoppieCoppie);
		listaMappe.add(mappaCoppie);
		listaMappe.add(mappaCarteAlte);

		System.out.println("Scale reali: " + mappaScaleReali.size());
		System.out.println("Scale Colori: " + mappaScaleColori.size());
		System.out.println("Poker: " + mappaPoker.size());
		System.out.println("Full: " + mappaFull.size());
		System.out.println("Colore: " + mappaColore.size());
		System.out.println("Scale Semplici: " + mappaScaleSemplici.size());
		System.out.println("Tris: " + mappaTris.size());
		System.out.println("Doppie Coppie: " + mappaDoppieCoppie.size());
		System.out.println("Coppie: " + mappaCoppie.size());
		System.out.println("Carte Più Alte: " + mappaCarteAlte.size());

		System.out.println(" ");

		/*
		 * Faccio una lista di mappe, la scorro e appena trovo una mappa che ha una
		 * lunghezza superiore a 0 se è 1 Prendo giocatore e mano di quella mappa e
		 * dichiaro il vincitore. altrimenti itero la mappa e vedo chi ha alla prima
		 * posizone delle carte, di quel dato tipo vincente, il valore più alto.
		 */
		for (int i = 0; i < listaMappe.size(); i++) {

			if (listaMappe.get(i).size() != 0) {
				if (listaMappe.get(i).size() > 1) {
					Iterator<Giocatore> iteratore = listaMappe.get(i).keySet().iterator();
					while (iteratore.hasNext()) {
						Giocatore giocatore = iteratore.next();
						EsitoManoGiocatore possibileManoVincente = listaMappe.get(i).get(giocatore);
						/*
						 * Se la condizione si verifica mettere dentro una lista possibiliManiVincenti
						 * che servirà poi per scorrerla e vedere chi ha la mano più altra tra quelle possibili vincenti
						 */
						if (possibileManoVincente.getTipoDiVincita() instanceof ScalaReale) {
							System.out.println(
									"Il Giocatore " + giocatore.getIndiceGiocatore() + " ha fatto Scala Reale");
							System.out.println("Con la seguente mano: " + giocatore.getManoGiocatore());
							System.out.println("Facendo: " + possibileManoVincente.getTipoDiVincita());
							possibiliCarteVincenti.add(possibileManoVincente.getTipoDiVincita());
							possibiliGiocatoriVincenti.add(giocatore);
						} else if (possibileManoVincente.getTipoDiVincita() instanceof ScalaColore) {
							System.out.println(
									"Il Giocatore " + giocatore.getIndiceGiocatore() + " ha fatto Scala Colore");
							System.out.println("Con la seguente mano: " + giocatore.getManoGiocatore());
							System.out.println("Facendo: " + possibileManoVincente.getTipoDiVincita());
							possibiliCarteVincenti.add(possibileManoVincente.getTipoDiVincita());
							possibiliGiocatoriVincenti.add(giocatore);
						} else if (possibileManoVincente.getTipoDiVincita() instanceof Poker) {
							System.out.println("Il Giocatore " + giocatore.getIndiceGiocatore() + " ha fatto Poker");
							System.out.println("Con la seguente mano: " + giocatore.getManoGiocatore());
							System.out.println("Facendo: " + possibileManoVincente.getTipoDiVincita());
							possibiliCarteVincenti.add(possibileManoVincente.getTipoDiVincita());
							possibiliGiocatoriVincenti.add(giocatore);
						} else if (possibileManoVincente.getTipoDiVincita() instanceof Full) {
							System.out.println("Il Giocatore " + giocatore.getIndiceGiocatore() + " ha fatto Full");
							System.out.println("Con la seguente mano: " + giocatore.getManoGiocatore());
							System.out.println("Facendo: " + possibileManoVincente.getTipoDiVincita());
							possibiliCarteVincenti.add(possibileManoVincente.getTipoDiVincita());
							possibiliGiocatoriVincenti.add(giocatore);
						} else if (possibileManoVincente.getTipoDiVincita() instanceof Colore) {
							System.out.println(
									"Il Giocatore " + giocatore.getIndiceGiocatore() + " ha fatto Colore");
							System.out.println("Con la seguente mano: " + giocatore.getManoGiocatore());
							System.out.println("Facendo: " + possibileManoVincente.getTipoDiVincita());
							possibiliCarteVincenti.add(possibileManoVincente.getTipoDiVincita());
							possibiliGiocatoriVincenti.add(giocatore);
						} else if (possibileManoVincente.getTipoDiVincita() instanceof Scala) {
							System.out.println(
									"Il Giocatore " + giocatore.getIndiceGiocatore() + " ha fatto Scala Semplice");
							System.out.println("Con la seguente mano: " + giocatore.getManoGiocatore());
							System.out.println("Facendo: " + possibileManoVincente.getTipoDiVincita());
							possibiliCarteVincenti.add(possibileManoVincente.getTipoDiVincita());
							possibiliGiocatoriVincenti.add(giocatore);
						} else if (possibileManoVincente.getTipoDiVincita() instanceof Tris) {
							System.out.println("Il Giocatore " + giocatore.getIndiceGiocatore() + " ha fatto Tris");
							System.out.println("Con la seguente mano: " + giocatore.getManoGiocatore());
							System.out.println("Facendo: " + possibileManoVincente.getTipoDiVincita());
							possibiliCarteVincenti.add(possibileManoVincente.getTipoDiVincita());
							possibiliGiocatoriVincenti.add(giocatore);
						} else if (possibileManoVincente.getTipoDiVincita() instanceof DoppiaCoppia) {
							System.out.println(
									"Il Giocatore " + giocatore.getIndiceGiocatore() + " ha fatto Doppia Coppia");
							System.out.println("Con la seguente mano: " + giocatore.getManoGiocatore());
							System.out.println("Facendo: " + possibileManoVincente.getTipoDiVincita());
							possibiliCarteVincenti.add(possibileManoVincente.getTipoDiVincita());
							possibiliGiocatoriVincenti.add(giocatore);
						} else if (possibileManoVincente.getTipoDiVincita() instanceof Coppia) {
							System.out.println("Il Giocatore " + giocatore.getIndiceGiocatore() + " ha fatto Coppia");
							System.out.println("Con la seguente mano: " + giocatore.getManoGiocatore());
							System.out.println("Facendo: " + possibileManoVincente.getTipoDiVincita());
							possibiliCarteVincenti.add(possibileManoVincente.getTipoDiVincita());
							possibiliGiocatoriVincenti.add(giocatore);
						} else if (possibileManoVincente.getTipoDiVincita() instanceof CartaPiuAlta) {
							System.out.println(
									"Il Giocatore " + giocatore.getIndiceGiocatore() + " ha fatto Carta Più Alta");
							System.out.println("Con la seguente mano: " + giocatore.getManoGiocatore());
							System.out.println("Facendo: " + possibileManoVincente.getTipoDiVincita());
							possibiliCarteVincenti.add(possibileManoVincente.getTipoDiVincita());
							possibiliGiocatoriVincenti.add(giocatore);
						}
						mappaCarteVincentiGiocatori.put(possibiliCarteVincenti, possibiliGiocatoriVincenti);
					}
					
					break;
				} else if (listaMappe.get(i).size() == 1) {
					/*
					 * Se la listaMappe è lunga una significa che già so chi è il vincitore perché non ci sono altre mani 
					 * più grandi oppure altre mani della stesso tipo che possono concorreree per la vincita
					 */
					
					Iterator<Giocatore> iteratore = listaMappe.get(i).keySet().iterator();
					Giocatore giocatore = iteratore.next();
					EsitoManoGiocatore manoVincente = listaMappe.get(i).get(giocatore);
					System.out.println("Ha vinto il giocatore " + giocatore.getIndiceGiocatore() + " facendo " + manoVincente.getTipoDiVincita());
					System.out.println("Con la seguente mano: " + giocatore.getManoGiocatore());
					break;
				}
			}
		}

		CartaPiuAlta cartaPiuAlta = null;
		Coppia coppiaPiuAlta = null;
		DoppiaCoppia doppiaCoppiaPiuAlta = null;
		Tris trisPiuAlto = null;
		Colore colorePiuAlto = null;
		Full fullPiuAlto = null;
		Poker pokerPiuAlto = null;
		Scala scalaSemplicePiuAlta = null;
		ScalaColore scalaColorePiuAlta = null;
		ScalaReale scalaRealePiuAlta = null;
		
		ControlloEsitoCoppia esitoCoppia = null;
		ControlloEsitoCartaAlta esitoCartaAlta = null;
		ControlloEsitoDoppiaCoppia esitoDoppiaCoppia = null;
		ControlloEsitoTris esitoTris = null;
		ControlloEsitoColore esitoColore = null;
		ControlloEsitoScala esitoScala = null;
		ControlloEsitoFull esitoFull = null;
		ControlloEsitoPoker esitoPoker = null;
		ControlloEsitoScalaColore esitoScalaColore = null;
		ControlloEsitoScalaReale esitoScalaReale = null;

		for(int i=0; i<possibiliCarteVincenti.size(); i++) {
				
				esitoScalaReale = new ControlloEsitoScalaReale(mappaCarteVincentiGiocatori);
				scalaRealePiuAlta = esitoScalaReale.valutaEsitoScalaRealePiuAlta(i);
				
				esitoScalaColore = new ControlloEsitoScalaColore(mappaCarteVincentiGiocatori);
				scalaColorePiuAlta = esitoScalaColore.valutaEsitoScalaColorePiuAlta(i);
				
				esitoPoker = new ControlloEsitoPoker(mappaCarteVincentiGiocatori);
				pokerPiuAlto = esitoPoker.valutaEsitoPokerPiuAlto(i);

				esitoFull = new ControlloEsitoFull(mappaCarteVincentiGiocatori);
				fullPiuAlto = esitoFull.valutaEsitoFullPiuAlto(i);

				esitoColore = new ControlloEsitoColore(mappaCarteVincentiGiocatori);
				colorePiuAlto = esitoColore.valutaEsitoColore(i);
				
				esitoScala = new ControlloEsitoScala(mappaCarteVincentiGiocatori);
				scalaSemplicePiuAlta = esitoScala.valutaEsitoScalaPiuAlta(i);
				
				esitoTris = new ControlloEsitoTris(mappaCarteVincentiGiocatori);
				trisPiuAlto = esitoTris.valutaEsitoTrisPiuAlto(i);
				
				esitoDoppiaCoppia = new ControlloEsitoDoppiaCoppia(mappaCarteVincentiGiocatori);
				doppiaCoppiaPiuAlta = esitoDoppiaCoppia.valutaEsitoTrisPiuAlto(i);
				
				esitoCoppia = new ControlloEsitoCoppia(mappaCarteVincentiGiocatori);
				coppiaPiuAlta = esitoCoppia.valutaEsitoCoppiAlta(i);

				esitoCartaAlta = new ControlloEsitoCartaAlta(mappaCarteVincentiGiocatori);
				cartaPiuAlta = esitoCartaAlta.valutaEsitoCartaAlta(i);
		}
		
		if(scalaRealePiuAlta!=null) {
			System.out.println("Ha vinto il giocatore " + esitoScalaReale.getGiocatore().getIndiceGiocatore() + 
				"con scala reale di "	+ scalaRealePiuAlta.getMano());
		}else if(scalaColorePiuAlta!=null) {
			System.out.println("Ha vinto Il giocatore " + esitoScalaColore.getGiocatore().getIndiceGiocatore() + 
					"con scala di " + scalaColorePiuAlta.getMano());
		}else if(pokerPiuAlto!=null) {
			System.out.println("Ha vinto il giocatore " + esitoPoker.getGiocatore().getIndiceGiocatore() +
					" con Poker di " + pokerPiuAlto.getCartePoker());
			System.out.println("Con la mano " + pokerPiuAlto.getMano());
		}else if(fullPiuAlto!=null) {
			System.out.println("Ha vinto il giocatore " + esitoFull.getGiocatore().getIndiceGiocatore() + 
					" con full di " + fullPiuAlto.getMano());
		}else if(colorePiuAlto!=null) {
			System.out.println("");
			System.out.println("Ha vinto il giocatore " + esitoColore.getGiocatore().getIndiceGiocatore() + 
					" con colore di " + colorePiuAlto.getSeme());
			System.out.println("Con la mano: " + colorePiuAlto.getMano());
		}else if(scalaSemplicePiuAlta!=null) {
			System.out.println("");
			System.out.println("Ha vinto il giocatore " + esitoScala.getGiocatore().getIndiceGiocatore() + 
					" con scala: " + scalaSemplicePiuAlta.getMano());
		}else if(trisPiuAlto!=null) {
			System.out.println("");
			System.out.println("Ha vinto il giocatore " + esitoTris.getGiocatore().getIndiceGiocatore() +
					" con tris di " +trisPiuAlto.getCarteTris());
			System.out.println("Con la mano: " + trisPiuAlto.getMano());
		}else if(doppiaCoppiaPiuAlta!=null) {  
			System.out.println("");
			System.out.println("Ha vinto il giocatore " + esitoDoppiaCoppia.getGiocatore().getIndiceGiocatore() +
					" con doppia coppia: " +doppiaCoppiaPiuAlta.getCarteDoppiaCoppia());
			System.out.println("Con la mano: " + doppiaCoppiaPiuAlta.getMano());
		}else if(coppiaPiuAlta!=null && coppiaPiuAlta!=null) { 
			System.out.println("");
			System.out.println("Ha vinto il giocatore " + esitoCoppia.getGiocatore().getIndiceGiocatore() +
					" con coppia di: " +coppiaPiuAlta.getCarteCoppia());
			System.out.println("Con la mano: " + coppiaPiuAlta.getMano());
		}else if(cartaPiuAlta!=null) {
			System.out.println("");
			System.out.println("Ha vinto il giocatore "+ esitoCartaAlta.getGiocatore().getIndiceGiocatore() + 
					" con carta alta: "+cartaPiuAlta.getCartaAlta());
			System.out.println("Con la mano: " + cartaPiuAlta.getMano());
		}
		
				/*
				 * Non rimane che capire se creare una Classe Vincitore a cui viene passato il giocatore vincitore
				 * e se creare altre singole classi per il controllo vincitore della singola tipologia di istanza
				 */
		}
	}


