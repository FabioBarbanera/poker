package controllaEsitoPartita.esito;

import java.util.ArrayList;
import java.util.List;

import giocatore.Giocatore;
import giocatore.mano.Carta;
import giocatore.mano.Mano;
import giocatore.mano.Mazziere;
import giocatore.mano.MazzoDiCarte;
import implMano.controllaPunteggioMano.Punteggio;
import utility.NumeroRandom;

public class Turno {

	private Turno() {
	}

	public static void avvia() {
		
		/*
		 * Preparo un mazzo di carte
		 */
		MazzoDiCarte mazzoDiCarte = MazzoDiCarte.creaMazzo();
		/*
		 * Ed un mazziere
		 */
		Mazziere mazziere = new Mazziere(mazzoDiCarte);
		
		/*
		 * Con la una classe statica di utility mi creo randomicamente la quantità di giocatori 
		 */
		Integer quantitaGiocatori = NumeroRandom.numeroRandomGiocatori();
		List<EsitoManoGiocatore> listaEsitiMani = new ArrayList<EsitoManoGiocatore>();
		List<Giocatore>listaGiocatori = new ArrayList<Giocatore>();
		Giocatore giocatore = null;
		Punteggio punteggioGiocatore = null;
		EsitoManoGiocatore esitoManoGiocatore = null;
		EsitoPartita esitoPartita = null;
		System.out.println(" ");
		/*
		 * Con un ciclo for lungo quanto la quantià di giocatori che sono
		 */
		for (int i = 0; i < quantitaGiocatori; i++) {
			/*
			 * Assegno ad ogni giocatore prima una mano
			 */
			Mano mano = mazziere.daiCarte();
			/*
			 * Poi un indice che è i + 1 perché parte da 0
			 */
			Integer indiceGiocatore = i + 1;
			/*
			 * Posso quindi crearmi il giocatore 
			 */
			giocatore = new Giocatore(mano, indiceGiocatore);
			/*
			 * Stampo la sua mano
			 */
			System.out.println("Mano giocatore " + indiceGiocatore);
			for (Carta carta : mano.getCarteDellaMano()) {
				System.out.print(carta + " ");
			}
			
			/*
			 * Posso controllare che tipologia di mano ha(es. se carta alta, coppia ecc.)
			 */
			punteggioGiocatore = new Punteggio(giocatore);
			esitoManoGiocatore = punteggioGiocatore.valuta();
			listaEsitiMani.add(esitoManoGiocatore);
			listaGiocatori.add(giocatore);
		}

		/*
		 * Quindi posso valutare l'esito della partita 
		 */
		esitoPartita = new EsitoPartita(listaEsitiMani, listaGiocatori);
		esitoPartita.valuta();

	}

}
