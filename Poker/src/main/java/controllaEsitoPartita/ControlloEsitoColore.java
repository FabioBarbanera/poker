package controllaEsitoPartita;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import giocatore.Giocatore;
import implMano.controllaPunteggioMano.risultatoMano.Colore;

public class ControlloEsitoColore {

	private List<Object> possibiliCarteVincenti;
	private List<Giocatore> giocatori;
	private Giocatore giocatore;
	private Colore colorePiuAlto;
	private Colore coloreSecondoPerConfornto;
	
	public ControlloEsitoColore(Map<List<Object>, List<Giocatore>>mappaCarteVincentiGiocatori) {
		
		Iterator <List<Object>> iteratore = mappaCarteVincentiGiocatori.keySet().iterator();
		this.possibiliCarteVincenti = iteratore.next();
		this.giocatori = mappaCarteVincentiGiocatori.get(this.possibiliCarteVincenti);
		
	}
	
	public Colore valutaEsitoColore(int i) {
		
		if (possibiliCarteVincenti.get(i) instanceof Colore) {
			/*
			 * In caso di due o più colori, vince quello con la carta più alta. In caso di
			 * parità, si mette a confronto la seconda carta più alta, poi la terza, e così
			 * via. In caso di ulteriore parità, si divide il piatto. Nella versione
			 * all'italiana, in caso di pareggio si segue la regola dei semi. Nel caso in
			 * cui ci siano due colori uguali (possibile solo dai sei giocatori in su), si
			 * prende in considerazione la carta più alta. (non possibile perche come numero
			 * di creazione dei giocatori random ho messo 5)
			 */

			for(int k=1; k<this.possibiliCarteVincenti.size();k++) {
				
				if (this.colorePiuAlto == null) {
					this.colorePiuAlto = (Colore) this.possibiliCarteVincenti.get(0);
				}
				
				if(this.giocatore == null){
					this.giocatore = this.giocatori.get(0);
				}

				this.coloreSecondoPerConfornto = (Colore) this.possibiliCarteVincenti.get(k);
				Integer valoreColorePiuAlto = this.colorePiuAlto.getMano().getCarteDellaMano()[4].getValoreCarta().getValore();
				Integer valoreColoreSecondoPerconfronto = this.coloreSecondoPerConfornto.getMano().getCarteDellaMano()[4].getValoreCarta().getValore();
					
				if (valoreColorePiuAlto < valoreColoreSecondoPerconfronto) {
					this.colorePiuAlto = this.coloreSecondoPerConfornto;
					this.giocatore = this.giocatori.get(k);
				}else if (valoreColorePiuAlto == valoreColoreSecondoPerconfronto) {
					
					for(int x=3; x>=0; x--) {
						valoreColorePiuAlto = this.colorePiuAlto.getMano().getCarteDellaMano()[x].getValoreCarta().getValore();
						valoreColoreSecondoPerconfronto = this.coloreSecondoPerConfornto.getMano().getCarteDellaMano()[x]
								.getValoreCarta().getValore();
						
						if (valoreColorePiuAlto > valoreColoreSecondoPerconfronto) {
							break;
						} else if (valoreColorePiuAlto < valoreColoreSecondoPerconfronto) {
							this.colorePiuAlto = this.coloreSecondoPerConfornto;
							this.giocatore = this.giocatori.get(k);
							break;
						} else if (valoreColorePiuAlto == valoreColoreSecondoPerconfronto) {
							
							if(x==0) {
								if (this.colorePiuAlto.getSeme().equals('♥')) {
									 break;
								} else if (this.coloreSecondoPerConfornto.getSeme().equals('♥')) {
									this.colorePiuAlto = this.coloreSecondoPerConfornto;
									this.giocatore = this.giocatori.get(k);
									break;
								} else if (this.colorePiuAlto.getSeme().equals('♦')) {
									break;
								} else if (this.coloreSecondoPerConfornto.getSeme().equals('♦')) {
									this.colorePiuAlto = this.coloreSecondoPerConfornto;
									this.giocatore = this.giocatori.get(k);
									break;
								} else if (this.colorePiuAlto.getSeme().equals('♣')) {
									break;
								} else if (this.coloreSecondoPerConfornto.getSeme().equals('♣')) {
									this.colorePiuAlto = this.coloreSecondoPerConfornto;
									this.giocatore = this.giocatori.get(k);
									break;
								} else if (this.colorePiuAlto.getSeme().equals('♠')) {
									break;
								} else if (this.coloreSecondoPerConfornto.getSeme().equals('♠')) {
									this.colorePiuAlto = this.coloreSecondoPerConfornto;
									this.giocatore = this.giocatori.get(k);
									break;
								}
							}
						}
					}
				}
			}
		}
		return this.colorePiuAlto;
	}
	
	public Giocatore getGiocatore() {
		return giocatore;
	}
	
}
